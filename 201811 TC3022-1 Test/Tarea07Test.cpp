#include "stdafx.h"
#include "CppUnitTest.h"
#include <cmath>
#include <limits>
#include <locale>
#include <codecvt>
#include "../201811 TC3022-1/transform.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Microsoft
{
	namespace VisualStudio
	{
		namespace CppUnitTestFramework
		{
			template<>
			static std::wstring ToString<mat4>(const mat4& m)
			{
				std::stringstream ss;
				ss << m;
				std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
				std::wstring wide = converter.from_bytes(ss.rdbuf()->str());
				return wide;
			}
		}
	}
}

TEST_CLASS(Tarea07Test)
{
public:
	TEST_METHOD(ConstructorGetModelMatrix)
	{
		transform t;
		mat4 modelMatrix = t.getModelMatrix();
		mat4 result(1.0f);
		Assert::AreEqual<mat4>(result, modelMatrix);
	}

	TEST_METHOD(ConstructorGetPosition)
	{
		transform t;
		vec3 position = t.getPosition();
		Assert::AreEqual(0.0f, position.x, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, position.y, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, position.z, std::numeric_limits<float>::epsilon());
	}

	TEST_METHOD(ConstructorGetRotation)
	{
		transform t;
		quat rotation = t.getRotation();
		Assert::AreEqual(0.0f, rotation.x, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, rotation.y, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, rotation.z, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(1.0f, rotation.w, std::numeric_limits<float>::epsilon());
	}

	TEST_METHOD(ConstructorGetScale)
	{
		transform t;
		vec3 scale = t.getScale();
		Assert::AreEqual(1.0f, scale.x, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(1.0f, scale.y, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(1.0f, scale.z, std::numeric_limits<float>::epsilon());
	}

	TEST_METHOD(SetGetPosition)
	{
		transform t;
		t.setPosition(1.0f, 2.0f, 3.0f);
		Assert::AreEqual(1.0f, t.getPosition().x, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(2.0f, t.getPosition().y, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(3.0f, t.getPosition().z, std::numeric_limits<float>::epsilon());
		mat4 result(1.0f);
		result[3][0] = 1.0f;
		result[3][1] = 2.0f;
		result[3][2] = 3.0f;
		Assert::AreEqual<mat4>(result, t.getModelMatrix());

		t.setPosition(-1.0f, -2.0f, -3.0f);
		Assert::AreEqual(-1.0f, t.getPosition().x, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(-2.0f, t.getPosition().y, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(-3.0f, t.getPosition().z, std::numeric_limits<float>::epsilon());
		mat4 result2(1.0f);
		result2[3][0] = -1.0f;
		result2[3][1] = -2.0f;
		result2[3][2] = -3.0f;
		Assert::AreEqual<mat4>(result2, t.getModelMatrix());

		t.setPosition(0.0f, 0.0f, 0.0f);
		Assert::AreEqual(0.0f, t.getPosition().x, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, t.getPosition().y, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, t.getPosition().z, std::numeric_limits<float>::epsilon());
		mat4 result3(1.0f);
		Assert::AreEqual<mat4>(result3, t.getModelMatrix());
	}

	TEST_METHOD(SetGetRotation)
	{
		transform t;
		t.setRotation(37.0f, -25.0f, 495.0f);
		Assert::AreEqual(-0.30818f, t.getRotation().x, 0.00001f);
		Assert::AreEqual(-0.20765f, t.getRotation().y, 0.00001f);
		Assert::AreEqual(-0.88165f, t.getRotation().z, 0.00001f);
		Assert::AreEqual(-0.29085f, t.getRotation().w, 0.00001f);
		mat4 modelMatrix = t.getModelMatrix();
		Assert::AreEqual(-0.640856f, modelMatrix[0][0], 0.00001f);
		Assert::AreEqual(0.640857f, modelMatrix[0][1], 0.00001f);
		Assert::AreEqual(0.422618f, modelMatrix[0][2], 0.00001f);
		Assert::AreEqual(-0.384877f, modelMatrix[1][0], 0.00001f);
		Assert::AreEqual(-0.744565f, modelMatrix[1][1], 0.00001f);
		Assert::AreEqual(0.54543f, modelMatrix[1][2], 0.00001f);
		Assert::AreEqual(0.664209f, modelMatrix[2][0], 0.00001f);
		Assert::AreEqual(0.186886f, modelMatrix[2][1], 0.00001f);
		Assert::AreEqual(0.72381f, modelMatrix[2][2], 0.00001f);

		t.setRotation(0.0f, 0.0f, 0.0f);
		Assert::AreEqual(0.0f, t.getRotation().x, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, t.getRotation().y, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, t.getRotation().z, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(1.0f, t.getRotation().w, std::numeric_limits<float>::epsilon());
		mat4 result3(1.0f);
		Assert::AreEqual<mat4>(result3, t.getModelMatrix());
	}

	TEST_METHOD(SetGetScale)
	{
		transform t;
		t.setScale(1.0f, 2.0f, 3.0f);
		Assert::AreEqual(1.0f, t.getScale().x, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(2.0f, t.getScale().y, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(3.0f, t.getScale().z, std::numeric_limits<float>::epsilon());
		mat4 result(1.0f);
		result[0][0] = 1.0f;
		result[1][1] = 2.0f;
		result[2][2] = 3.0f;
		Assert::AreEqual<mat4>(result, t.getModelMatrix());

		t.setScale(-1.0f, -2.0f, -3.0f);
		Assert::AreEqual(-1.0f, t.getScale().x, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(-2.0f, t.getScale().y, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(-3.0f, t.getScale().z, std::numeric_limits<float>::epsilon());
		mat4 result2(1.0f);
		result2[0][0] = -1.0f;
		result2[1][1] = -2.0f;
		result2[2][2] = -3.0f;
		Assert::AreEqual<mat4>(result2, t.getModelMatrix());

		t.setScale(0.0f, 0.0f, 0.0f);
		Assert::AreEqual(0.0f, t.getScale().x, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, t.getScale().y, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, t.getScale().z, std::numeric_limits<float>::epsilon());
		mat4 result3(0.0f);
		result3[3][3] = 1.0f;
		Assert::AreEqual<mat4>(result3, t.getModelMatrix());
	}
};