#include "stdafx.h"
#include "CppUnitTest.h"
#include <cmath>
#include <limits>
#include <locale>
#include <codecvt>
#include "../201811 TC3022-1/vec2.h"
#include "../201811 TC3022-1/vec3.h"
#include "../201811 TC3022-1/vec4.h"
#include "../201811 TC3022-1/mat3.h"
#include "../201811 TC3022-1/mat4.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Microsoft
{
	namespace VisualStudio
	{
		namespace CppUnitTestFramework
		{
			template<>
			static std::wstring ToString<vec2>(const vec2& v)
			{
				return L"(" + std::to_wstring(v.x) + L", " + std::to_wstring(v.y) + L")";
			}

			template<>
			static std::wstring ToString<vec3>(const vec3& v)
			{
				return L"(" + std::to_wstring(v.x) + L", " + std::to_wstring(v.y) + L", " + std::to_wstring(v.z) + L")";
			}

			template<>
			static std::wstring ToString<vec4>(const vec4& v)
			{
				return L"(" + std::to_wstring(v.x) + L", " + std::to_wstring(v.y) + L", " + std::to_wstring(v.z) + L", " + std::to_wstring(v.w) + L")";
			}

			template<>
			static std::wstring ToString<mat3>(const mat3& m)
			{
				std::stringstream ss;
				ss << m;
				std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
				std::wstring wide = converter.from_bytes(ss.rdbuf()->str());
				return wide;
			}

			template<>
			static std::wstring ToString<mat4>(const mat4& m)
			{
				std::stringstream ss;
				ss << m;
				std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
				std::wstring wide = converter.from_bytes(ss.rdbuf()->str());
				return wide;
			}
		}
	}
}

TEST_CLASS(ExamVec2Test)
{
public:

	TEST_METHOD(ConstructorTest)
	{
		vec2 a;
		Assert::AreEqual(0.0f, a.x, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, a.y, std::numeric_limits<float>::epsilon());

		vec2 b(2.0f, -4.0f);
		Assert::AreEqual(2.0f, b.x, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(-4.0f, b.y, std::numeric_limits<float>::epsilon());
	}

	TEST_METHOD(IndexTest)
	{
		vec2 a;
		Assert::AreEqual(0.0f, a[0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, a[1], std::numeric_limits<float>::epsilon());

		a[0] = -1.0f;
		a[1] = 3.0f;
		Assert::AreEqual(-1.0f, a[0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(3.0f, a[1], std::numeric_limits<float>::epsilon());

		vec2 b(-5.0f, 19.0f);
		Assert::AreEqual(-5.0f, b[0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(19.0f, b[1], std::numeric_limits<float>::epsilon());

		b[0] = 10.0f;
		b[1] = -6.0f;
		Assert::AreEqual(10.0f, b[0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(-6.0f, b[1], std::numeric_limits<float>::epsilon());
	}

	TEST_METHOD(OperatorTest)
	{
		vec2 a;

		a *= 5.0f;
		Assert::AreEqual(0.0f, a[0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, a[1], std::numeric_limits<float>::epsilon());

		a /= 2.0f;
		Assert::AreEqual(0.0f, a[0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, a[1], std::numeric_limits<float>::epsilon());

		vec2 b(2.0f, -3.0f);

		a += b;
		Assert::AreEqual(2.0f, a[0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(-3.0f, a[1], std::numeric_limits<float>::epsilon());

		b -= a;
		Assert::AreEqual(0.0f, b[0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, b[1], std::numeric_limits<float>::epsilon());

		vec2 c(1.0f, -2.0f);
		vec2 d(-10.0f, 20.0f);

		c *= 5.0f;
		Assert::AreEqual(5.0f, c[0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(-10.0f, c[1], std::numeric_limits<float>::epsilon());

		d /= -2.0f;
		Assert::AreEqual(5.0f, d[0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(-10.0f, d[1], std::numeric_limits<float>::epsilon());

		Assert::AreEqual<vec2>(c, d);
		Assert::AreNotEqual<vec2>(a, c);

		std::stringstream ss;
		ss << d;
		std::string vecstring = ss.rdbuf()->str();
		Assert::AreEqual<std::string>("(5, -10)", vecstring);

		a = d * 5.0f;
		Assert::AreEqual(25.0f, a[0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(-50.0f, a[1], std::numeric_limits<float>::epsilon());

		b = 5.0f * d;
		Assert::AreEqual(25.0f, b[0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(-50.0f, b[1], std::numeric_limits<float>::epsilon());

		c = a / 5.0f;
		Assert::AreEqual(5.0f, c[0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(-10.0f, c[1], std::numeric_limits<float>::epsilon());

		d = a + c;
		Assert::AreEqual(30.0f, d[0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(-60.0f, d[1], std::numeric_limits<float>::epsilon());

		d = d - b;
		Assert::AreEqual(5.0f, d[0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(-10.0f, d[1], std::numeric_limits<float>::epsilon());

		Assert::AreEqual<vec2>(vec2(-5.0f, 10.0f), -d);

		a = -(a + b - c + d) * 0.5f;
		Assert::AreEqual(a.x, -25.0f, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(a.y, 50.0f, std::numeric_limits<float>::epsilon());
	}

	TEST_METHOD(MagnitudeTest)
	{
		vec2 a;
		Assert::AreEqual(0.0f, a.magnitude(), std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, vec2::magnitude(a), std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, a.x, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, a.y, std::numeric_limits<float>::epsilon());

		vec2 b(1.0f, 0.0f);
		Assert::AreEqual(1.0f, b.magnitude(), std::numeric_limits<float>::epsilon());
		Assert::AreEqual(1.0f, vec2::magnitude(b), std::numeric_limits<float>::epsilon());
		Assert::AreEqual(1.0f, b.x, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, b.y, std::numeric_limits<float>::epsilon());

		vec2 c(0.0f, 1.0f);
		Assert::AreEqual(1.0f, c.magnitude(), std::numeric_limits<float>::epsilon());
		Assert::AreEqual(1.0f, vec2::magnitude(c), std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, c.x, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(1.0f, c.y, std::numeric_limits<float>::epsilon());

		vec2 d(5.0f, -4.0f);
		Assert::AreEqual(sqrt(41.0f), d.magnitude(), std::numeric_limits<float>::epsilon());
		Assert::AreEqual(sqrt(41.0f), vec2::magnitude(d), std::numeric_limits<float>::epsilon());
		Assert::AreEqual(5.0f, d.x, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(-4.0f, d.y, std::numeric_limits<float>::epsilon());
	}

	TEST_METHOD(NormalizeTest)
	{
		vec2 a;
		vec2 b = vec2::normalize(a);
		Assert::IsTrue(std::isnan(b.x));
		Assert::IsTrue(std::isnan(b.y));
		a.normalize();
		Assert::IsTrue(std::isnan(a.x));
		Assert::IsTrue(std::isnan(a.y));

		vec2 c(1.0f, 0.0f);
		Assert::AreEqual(1.0f, vec2::normalize(c).x, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, vec2::normalize(c).y, std::numeric_limits<float>::epsilon());
		c.normalize();
		Assert::AreEqual(1.0f, c.x, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, c.y, std::numeric_limits<float>::epsilon());

		vec2 d(0.0f, 1.0f);
		Assert::AreEqual(0.0f, vec2::normalize(d).x, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(1.0f, vec2::normalize(d).y, std::numeric_limits<float>::epsilon());
		d.normalize();
		Assert::AreEqual(0.0f, d.x, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(1.0f, d.y, std::numeric_limits<float>::epsilon());

		vec2 e(1.0f, -4.0f);
		Assert::AreEqual(1.0f / sqrt(17.0f), vec2::normalize(e).x, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(-4.0f / sqrt(17.0f), vec2::normalize(e).y, std::numeric_limits<float>::epsilon());
		e.normalize();
		Assert::AreEqual(1.0f / sqrt(17.0f), e.x, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(-4.0f / sqrt(17.0f), e.y, std::numeric_limits<float>::epsilon());
	}

	TEST_METHOD(DotTest)
	{
		vec2 a;
		vec2 b;
		Assert::AreEqual(0.0f, vec2::dot(a, b), std::numeric_limits<float>::epsilon());

		a.x = 1.0f;
		a.y = 7.0f;
		Assert::AreEqual(0.0f, vec2::dot(a, b), std::numeric_limits<float>::epsilon());

		b.x = -3.0f;
		b.y = -2.0f;
		Assert::AreEqual(-17.0f, vec2::dot(a, b), std::numeric_limits<float>::epsilon());
	}
};

TEST_CLASS(ExamMat3Test)
{
	TEST_METHOD(DeterminantTest)
	{
		mat3 a;
		Assert::AreEqual(0.0f, mat3::determinant(a), std::numeric_limits<float>::epsilon());

		mat3 b(3.0f);
		Assert::AreEqual(27.0f, mat3::determinant(b), std::numeric_limits<float>::epsilon());

		vec3 c1(1.0f, -4.0f, 7.0f);
		vec3 c2(2.0f, 5.0f, 8.0f);
		vec3 c3(-3.0f, 6.0f, 9.0f);
		mat3 c(c1, c2, c3);
		Assert::AreEqual(354.0f, mat3::determinant(c), std::numeric_limits<float>::epsilon());

		vec3 d1(2.0f, 2.0f, 2.0f);
		mat3 d(d1, d1, d1);
		Assert::AreEqual(0.0f, mat3::determinant(d), std::numeric_limits<float>::epsilon());
	}

	TEST_METHOD(Inverse)
	{
		mat3 a;
		for (auto& i : { 0, 1, 2 })
			for (auto& j : { 0, 1, 2 })
				Assert::IsTrue(std::isnan(mat3::inverse(a)[i][j]));

		mat3 b(1.0f);
		Assert::AreEqual<mat3>(b, mat3::inverse(b));

		mat3 c(2.0f);
		mat3 d(0.5f);
		Assert::AreEqual<mat3>(d, mat3::inverse(c));

		vec3 e1(1.0f, 4.0f, -7.0f);
		vec3 e2(2.0f, -5.0f, 8.0f);
		vec3 e3(3.0f, 6.0f, 9.0f);
		mat3 e(e1, e2, e3);
		mat3 f = mat3::inverse(e);
		Assert::AreEqual(31.0f / 86.0f, f[0][0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(13.0f / 43.0f, f[0][1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(1.0f / 86.0f, f[0][2], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(-1.0f / 43.0f, f[1][0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(-5.0f / 43.0f, f[1][1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(11.0f / 129.0f, f[1][2], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(-9.0f / 86.0f, f[2][0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(-1.0f / 43.0f, f[2][1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(13.0f / 258.0f, f[2][2], std::numeric_limits<float>::epsilon());
	}

	TEST_METHOD(Transpose)
	{
		mat3 a;
		mat3 b = mat3::transpose(a);
		for (auto& i : { 0, 1, 2 })
			for (auto& j : { 0, 1, 2 })
				Assert::AreEqual(0.0f, b[i][j], std::numeric_limits<float>::epsilon());

		mat3 c(1.0f);
		Assert::AreEqual<mat3>(c, mat3::transpose(c));

		vec3 d1(1.0f, 2.0f, 3.0f);
		vec3 d2(-4.0f, -5.0f, -6.0f);
		vec3 d3(7.0f, 8.0f, 9.0f);
		mat3 d(d1, d2, d3);
		mat3 e = mat3::transpose(d);
		Assert::AreEqual(1.0f, e[0][0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(-4.0f, e[0][1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(7.0f, e[0][2], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(2.0f, e[1][0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(-5.0f, e[1][1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(8.0f, e[1][2], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(3.0f, e[2][0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(-6.0f, e[2][1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(9.0f, e[2][2], std::numeric_limits<float>::epsilon());
	}
};

TEST_CLASS(ExamVec4Test)
{
public:
	TEST_METHOD(ConstructorTest)
	{
		vec4 a;
		Assert::AreEqual(0.0f, a.x, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, a.y, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, a.z, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, a.w, std::numeric_limits<float>::epsilon());

		vec4 b(2.0f, -4.0f, 1.0f, 0.0f);
		Assert::AreEqual(2.0f, b.x, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(-4.0f, b.y, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(1.0f, b.z, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, b.w, std::numeric_limits<float>::epsilon());
	}

	TEST_METHOD(IndexTest)
	{
		vec4 a;
		Assert::AreEqual(0.0f, a[0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, a[1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, a[2], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, a[3], std::numeric_limits<float>::epsilon());

		a[0] = -1.0f;
		a[1] = 3.0f;
		a[2] = 2.0f;
		a[3] = 16.0f;
		Assert::AreEqual(-1.0f, a[0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(3.0f, a[1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(2.0f, a[2], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(16.0f, a[3], std::numeric_limits<float>::epsilon());

		vec4 b(-5.0f, 19.0f, 0.0f, 0.33f);
		Assert::AreEqual(-5.0f, b[0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(19.0f, b[1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, b[2], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.33f, b[3], std::numeric_limits<float>::epsilon());

		b[0] = 10.0f;
		b[1] = -6.0f;
		b[2] = 9.0f;
		b[3] = -5.0f;
		Assert::AreEqual(10.0f, b[0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(-6.0f, b[1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(9.0f, b[2], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(-5.0f, b[3], std::numeric_limits<float>::epsilon());
	}

	TEST_METHOD(OperatorTest)
	{
		vec4 a;

		a *= 5.0f;
		Assert::AreEqual(0.0f, a[0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, a[1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, a[2], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, a[3], std::numeric_limits<float>::epsilon());

		a /= 2.0f;
		Assert::AreEqual(0.0f, a[0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, a[1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, a[2], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, a[3], std::numeric_limits<float>::epsilon());

		vec4 b(2.0f, -3.0f, 1.0f, -5.0f);

		a += b;
		Assert::AreEqual(2.0f, a[0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(-3.0f, a[1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(1.0f, a[2], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(-5.0f, a[3], std::numeric_limits<float>::epsilon());

		b -= a;
		Assert::AreEqual(0.0f, b[0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, b[1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, b[2], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, b[3], std::numeric_limits<float>::epsilon());

		vec4 c(1.0f, -2.0f, 3.0f, -4.0f);
		vec4 d(-10.0f, 20.0f, -30.0f, 40.0f);

		c *= 5.0f;
		Assert::AreEqual(5.0f, c[0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(-10.0f, c[1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(15.0f, c[2], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(-20.0f, c[3], std::numeric_limits<float>::epsilon());

		d /= -2.0f;
		Assert::AreEqual(5.0f, d[0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(-10.0f, d[1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(15.0f, d[2], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(-20.0f, d[3], std::numeric_limits<float>::epsilon());

		Assert::AreEqual<vec4>(c, d);
		Assert::AreNotEqual<vec4>(a, c);

		std::stringstream ss;
		ss << d;
		std::string vecstring = ss.rdbuf()->str();
		Assert::AreEqual<std::string>("(5, -10, 15, -20)", vecstring);

		a = d * 5.0f;
		Assert::AreEqual(25.0f, a[0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(-50.0f, a[1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(75.0f, a[2], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(-100.0f, a[3], std::numeric_limits<float>::epsilon());

		b = 5.0f * d;
		Assert::AreEqual(25.0f, b[0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(-50.0f, b[1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(75.0f, b[2], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(-100.0f, b[3], std::numeric_limits<float>::epsilon());

		c = a / 5.0f;
		Assert::AreEqual(5.0f, c[0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(-10.0f, c[1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(15.0f, c[2], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(-20.0f, c[3], std::numeric_limits<float>::epsilon());

		d = a + c;
		Assert::AreEqual(30.0f, d[0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(-60.0f, d[1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(90.0f, d[2], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(-120.0f, d[3], std::numeric_limits<float>::epsilon());

		d = d - b;
		Assert::AreEqual(5.0f, d[0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(-10.0f, d[1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(15.0f, d[2], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(-20.0f, d[3], std::numeric_limits<float>::epsilon());

		Assert::AreEqual<vec4>(vec4(-5.0f, 10.0f, -15.0f, 20.0f), -d);

		a = -(a + b - c + d) * 0.5f;
		Assert::AreEqual(-25.0f, a.x, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(50.0f, a.y, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(-75.0f, a.z, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(100.0f, a.w, std::numeric_limits<float>::epsilon());
	}
};

TEST_CLASS(ExamMat4Test)
{
public:
	TEST_METHOD(ConstructorTest)
	{
		mat4 a;
		Assert::AreEqual(0.0f, a[0][0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, a[0][1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, a[0][2], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, a[0][3], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, a[1][0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, a[1][1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, a[1][2], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, a[1][3], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, a[2][0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, a[2][1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, a[2][2], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, a[2][3], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, a[3][0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, a[3][1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, a[3][2], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, a[3][3], std::numeric_limits<float>::epsilon());

		mat4 b(1.0f);
		Assert::AreEqual(1.0f, b[0][0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, b[0][1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, b[0][2], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, b[0][3], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, b[1][0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(1.0f, b[1][1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, b[1][2], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, b[1][3], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, b[2][0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, b[2][1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(1.0f, b[2][2], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, b[2][3], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, b[3][0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, b[3][1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, b[3][2], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(1.0f, b[3][3], std::numeric_limits<float>::epsilon());

		vec4 col1(0.0f, 1.0f, 2.0f, 3.0f);
		vec4 col2(4.0f, 5.0f, 6.0f, 7.0f);
		vec4 col3(8.0f, 9.0f, 10.0f, 11.0f);
		vec4 col4(12.0f, 13.0f, 14.0f, 15.0f);
		mat4 c(col1, col2, col3, col4);
		for (auto& col : { 0, 1, 2, 3 })
			for (auto& row : { 0, 1, 2, 3 })
				Assert::AreEqual(col * 4.0f + row, c[col][row], std::numeric_limits<float>::epsilon());
	}

	TEST_METHOD(IndexTest)
	{
		vec4 col1(0.0f, 1.0f, 2.0f, 3.0f);
		vec4 col2(4.0f, 5.0f, 6.0f, 7.0f);
		vec4 col3(8.0f, 9.0f, 10.0f, 11.0f);
		vec4 col4(12.0f, 13.0f, 14.0f, 15.0f);
		mat4 a(col1, col2, col3, col4);
		for (auto& col : { 0, 1, 2, 3 })
			for (auto& row : { 0, 1, 2, 3 })
				Assert::AreEqual(col * 4.0f + row, a[col][row], std::numeric_limits<float>::epsilon());

		a[0][0] = 1.0f + 22.0f;	a[1][0] -= 2.0f;		a[2][0] = 7.0f * 3.0f;	a[3][0] = 1.0f + 22.0f;
		a[0][1] = 22.0f - 3.0f;	a[1][1] += 11.0f;		a[2][1] *= 3.0f;		a[3][1] = 22.0f - 3.0f;
		a[0][2] = 18.0f;		a[1][2] = 6.0f / 3.0f;	a[2][2] /= 3.0f;		a[3][2] = 18.0f;
		a[0][3] = 17.0f;		a[1][3] = 9.0f / 3.0f;	a[2][3] /= 3.0f;		a[3][3] = 38.0f;

		Assert::AreEqual(1.0f + 22.0f, a[0][0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(22.0f - 3.0f, a[0][1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(18.0f, a[0][2], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(17.0f, a[0][3], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(4.0f - 2.0f, a[1][0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(5.0f + 11.0f, a[1][1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(6.0f / 3.0f, a[1][2], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(9.0f / 3.0f, a[1][3], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(7.0f * 3.0f, a[2][0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(9.0f * 3.0f, a[2][1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(10.0f / 3.0f, a[2][2], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(11.0f / 3.0f, a[2][3], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(1.0f + 22.0f, a[3][0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(22.0f - 3.0f, a[3][1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(18.0f, a[3][2], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(38.0f, a[3][3], std::numeric_limits<float>::epsilon());
	}

	TEST_METHOD(Mat4xMat4)
	{
		mat4 m1;
		mat4 m2(1.0f);

		Assert::AreEqual<mat4>(m1, m1 * m2);
		Assert::AreNotEqual<mat4>(m2, m2 * m1);

		m1[0][0] = 1.0f;	m1[1][0] = 2.0f;	m1[2][0] = 3.0f;	m1[3][0] = 4.0f;
		m1[0][1] = 5.0f;	m1[1][1] = 6.0f;	m1[2][1] = 7.0f;	m1[3][1] = 8.0f;
		m1[0][2] = 9.0f;	m1[1][2] = 10.0f;	m1[2][2] = 11.0f;	m1[3][2] = 12.0f;
		m1[0][3] = 13.0f;	m1[1][3] = 14.0f;	m1[2][3] = 15.0f;	m1[3][3] = 16.0f;

		m2[0][0] = -1.0f;	m2[1][0] = -2.0f;	m2[2][0] = -3.0f;	m2[3][0] = -4.0f;
		m2[0][1] = -5.0f;	m2[1][1] = -6.0f;	m2[2][1] = -7.0f;	m2[3][1] = -8.0f;
		m2[0][2] = -9.0f;	m2[1][2] = -10.0f;	m2[2][2] = -11.0f;	m2[3][2] = -12.0f;
		m2[0][3] = -13.0f;	m2[1][3] = -14.0f;	m2[2][3] = -15.0f;	m2[3][3] = -16.0f;

		mat4 r;
		r[0][0] = -90.0f;	r[1][0] = -100.0f;	r[2][0] = -110.0f;	r[3][0] = -120.0f;
		r[0][1] = -202.0f;	r[1][1] = -228.0f;	r[2][1] = -254.0f;	r[3][1] = -280.0f;
		r[0][2] = -314.0f;	r[1][2] = -356.0f;	r[2][2] = -398.0f;	r[3][2] = -440.0f;
		r[0][3] = -426.0f;	r[1][3] = -484.0f;	r[2][3] = -542.0f;	r[3][3] = -600.0f;
		Assert::AreEqual<mat4>(r, m1 * m2);

		m1[0][0] = 1.0f;	m1[1][0] = 2.0f;	m1[2][0] = 3.0f;	m1[3][0] = 4.0f;
		m1[0][1] = 5.0f;	m1[1][1] = 6.0f;	m1[2][1] = 7.0f;	m1[3][1] = 8.0f;
		m1[0][2] = 9.0f;	m1[1][2] = 10.0f;	m1[2][2] = 11.0f;	m1[3][2] = 12.0f;
		m1[0][3] = 13.0f;	m1[1][3] = 14.0f;	m1[2][3] = 15.0f;	m1[3][3] = 16.0f;

		m2[0][0] = 17.0f;	m2[1][0] = 18.0f;	m2[2][0] = 19.0f;	m2[3][0] = 20.0f;
		m2[0][1] = 21.0f;	m2[1][1] = 22.0f;	m2[2][1] = 23.0f;	m2[3][1] = 24.0f;
		m2[0][2] = 25.0f;	m2[1][2] = 26.0f;	m2[2][2] = 27.0f;	m2[3][2] = 28.0f;
		m2[0][3] = 29.0f;	m2[1][3] = 30.0f;	m2[2][3] = 31.0f;	m2[3][3] = 32.0f;

		r[0][0] = 250.0f;	r[1][0] = 260.0f;	r[2][0] = 270.0f;	r[3][0] = 280.0f;
		r[0][1] = 618.0f;	r[1][1] = 644.0f;	r[2][1] = 670.0f;	r[3][1] = 696.0f;
		r[0][2] = 986.0f;	r[1][2] = 1028.0f;	r[2][2] = 1070.0f;	r[3][2] = 1112.0f;
		r[0][3] = 1354.0f;	r[1][3] = 1412.0f;	r[2][3] = 1470.0f;	r[3][3] = 1528.0f;
		Assert::AreEqual<mat4>(r, m1 * m2);
		Assert::AreNotEqual<mat4>(r, m2 * m1);

		r[0][0] = 538.0f;	r[1][0] = 612.0f;	r[2][0] = 686.0f;	r[3][0] = 760.0f;
		r[0][1] = 650.0f;	r[1][1] = 740.0f;	r[2][1] = 830.0f;	r[3][1] = 920.0f;
		r[0][2] = 762.0f;	r[1][2] = 868.0f;	r[2][2] = 974.0f;	r[3][2] = 1080.0f;
		r[0][3] = 874.0f;	r[1][3] = 996.0f;	r[2][3] = 1118.0f;	r[3][3] = 1240.0f;
		Assert::AreEqual<mat4>(r, m2 * m1);

		vec4 v(2.0f, 0.0f, -1.0f, 1.0f);

		float pi = 3.1415926535897932384626433832795f;
		m1[0][0] = cos(pi);	m1[1][0] = -sin(pi);	m1[2][0] = 0.0f;	m1[3][0] = 0.0f;
		m1[0][1] = sin(pi);	m1[1][1] = cos(pi);		m1[2][1] = 0.0f;	m1[3][1] = 0.0f;
		m1[0][2] = 0.0f;	m1[1][2] = 0.0f;		m1[2][2] = 1.0f;	m1[3][2] = 0.0f;
		m1[0][3] = 0.0f;	m1[1][3] = 0.0f;		m1[2][3] = 0.0f;	m1[3][3] = 1.0f;

		m2[0][0] = cos(pi);	m2[1][0] = -sin(pi);	m2[2][0] = 0.0f;	m2[3][0] = 0.0f;
		m2[0][1] = sin(pi);	m2[1][1] = cos(pi);		m2[2][1] = 0.0f;	m2[3][1] = 0.0f;
		m2[0][2] = 0.0f;	m2[1][2] = 0.0f;		m2[2][2] = 1.0f;	m2[3][2] = 0.0f;
		m2[0][3] = 0.0f;	m2[1][3] = 0.0f;		m2[2][3] = 0.0f;	m2[3][3] = 1.0f;

		vec4 res = m1 * m2 * v;
		Assert::AreEqual(2.0f, v.x, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, v.y, 0.000001f);
		Assert::AreEqual(-1.0f, v.z, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(1.0f, v.w, std::numeric_limits<float>::epsilon());
	}

	TEST_METHOD(Mat4xVec4)
	{
		vec3 v1(3.0f, 6.0f, 4.0f);
		mat3 m(1.0f);
		Assert::AreEqual<vec3>(v1, m * v1);

		m[0][0] *= 3.0f;
		m[1][1] *= 3.0f;
		m[2][2] *= 3.0f;
		Assert::AreEqual<vec3>(v1 * 3.0f, m * v1);

		vec3 v2(2.0f, -40.0f, 1.0f);
		m[2][0] = v2.x;
		m[2][1] = v2.y;
		m[2][2] = v2.z;
		Assert::AreEqual<vec3>(vec3(17.0f, -142.0f, 4.0f), m * v1);

		vec3 v3(-3.0f, 0.0f, 8.0f);
		m[1][0] = -2.0f;
		m[0][1] = -3.0f;
		m[0][2] = -7.0f;
		Assert::AreEqual<vec3>(vec3(7.0f, -311.0f, 29.0f), m * v3);

		vec3 v4;
		Assert::AreEqual<vec3>(vec3(), m * v4);

		mat3 m2;
		Assert::AreEqual<vec3>(v4, m2 * v2);

		v4[0] = 1.0f;
		v4[1] = 0.0f;
		v4[2] = 1.0f;

		m2[0][0] = cos(3.14159f);	m2[1][0] = -sin(3.14159f);	m2[2][0] = 0.0f;
		m2[0][1] = sin(3.14159f);	m2[1][1] = cos(3.14159f);	m2[2][1] = 0.0f;
		m2[0][2] = 0.0f;			m2[1][2] = 0.0f;			m2[2][2] = 1.0f;
		Assert::AreEqual(-1.0f, (m2 * v4).x, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, (m2 * v4).y, 0.00001f);
		Assert::AreEqual(1.0f, (m2 * v4).z, std::numeric_limits<float>::epsilon());
	}
};