#include "stdafx.h"
#include "CppUnitTest.h"
#include <cmath>
#include <limits>
#include "../201811 TC3022-1/vec3.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Microsoft {
	namespace VisualStudio {
		namespace CppUnitTestFramework {
			template<>
			static std::wstring ToString<vec3>(const vec3& v) {
				return L"(" + std::to_wstring(v.x) + L", " + std::to_wstring(v.y) + L", " + std::to_wstring(v.z) + L")";
			}

		}
	}
}

TEST_CLASS(Vec3Test)
{
public:

	TEST_METHOD(ConstructorTest)
	{
		vec3 a;
		Assert::AreEqual(0.0f, a.x, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, a.y, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, a.z, std::numeric_limits<float>::epsilon());

		vec3 b(2.0f, 3.0f, 4.0f);
		Assert::AreEqual(2.0f, b.x, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(3.0f, b.y, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(4.0f, b.z, std::numeric_limits<float>::epsilon());
	}

	TEST_METHOD(MagnitudeTest)
	{
		vec3 a;
		Assert::AreEqual(0.0f, a.magnitude(), std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, vec3::magnitude(a), std::numeric_limits<float>::epsilon());

		vec3 b(1.0f, 0.0f, 0.0f);
		Assert::AreEqual(1.0f, b.magnitude(), std::numeric_limits<float>::epsilon());
		Assert::AreEqual(1.0f, vec3::magnitude(b), std::numeric_limits<float>::epsilon());

		vec3 c(0.0f, 1.0f, 0.0f);
		Assert::AreEqual(1.0f, c.magnitude(), std::numeric_limits<float>::epsilon());
		Assert::AreEqual(1.0f, vec3::magnitude(c), std::numeric_limits<float>::epsilon());

		vec3 d(0.0f, 0.0f, 1.0f);
		Assert::AreEqual(1.0f, c.magnitude(), std::numeric_limits<float>::epsilon());
		Assert::AreEqual(1.0f, vec3::magnitude(c), std::numeric_limits<float>::epsilon());

		vec3 e(5.0f, 4.0f, 2.0f);
		Assert::AreEqual(sqrt(45.0f), e.magnitude(), std::numeric_limits<float>::epsilon());
		Assert::AreEqual(sqrt(45.0f), vec3::magnitude(e), std::numeric_limits<float>::epsilon());
	}

	TEST_METHOD(NormalizeTest)
	{
		vec3 a;
		vec3 b = vec3::normalize(a);
		Assert::IsTrue(std::isnan(b.x));
		Assert::IsTrue(std::isnan(b.y));
		Assert::IsTrue(std::isnan(b.z));
		a.normalize();
		Assert::IsTrue(std::isnan(a.x));
		Assert::IsTrue(std::isnan(a.y));
		Assert::IsTrue(std::isnan(a.z));

		vec3 c(1.0f, 0.0f, 0.0f);
		Assert::AreEqual<vec3>(vec3(1.0f, 0.0f, 0.0f), vec3::normalize(c));
		c.normalize();
		Assert::AreEqual<vec3>(vec3(1.0f, 0.0f, 0.0f), c);

		vec3 d(0.0f, 1.0f, 0.0f);
		Assert::AreEqual<vec3>(vec3(0.0f, 1.0f, 0.0f), vec3::normalize(d));
		d.normalize();
		Assert::AreEqual<vec3>(vec3(0.0f, 1.0f, 0.0f), d);

		vec3 e(0.0f, 0.0f, 1.0f);
		Assert::AreEqual<vec3>(vec3(0.0f, 0.0f, 1.0f), vec3::normalize(e));
		e.normalize();
		Assert::AreEqual<vec3>(vec3(0.0f, 0.0f, 1.0f), e);

		vec3 f(1.0f, 4.0f, 8.0f);
		Assert::AreEqual<vec3>(vec3(1.0f / 9.0f, 4.0f / 9.0f, 8.0f / 9.0f), vec3::normalize(f));
		f.normalize();
		Assert::AreEqual<vec3>(vec3(1.0f / 9.0f, 4.0f / 9.0f, 8.0f / 9.0f), f);
	}

	TEST_METHOD(DotTest)
	{
		vec3 a(1.0f, 0.0f, 0.0f);
		vec3 b(-1.0f, 0.0f, 0.0f);
		vec3 c(0.0f, 1.0f, 0.0f);
		vec3 d(0.0f, -1.0f, 0.0f);
		vec3 e(0.0f, 0.0, 1.0f);
		vec3 f(0.0f, 0.0f, -1.0f);

		Assert::AreEqual(1.0f, vec3::dot(a, a), std::numeric_limits<float>::epsilon());
		Assert::AreEqual(-1.0f, vec3::dot(a, b), std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, vec3::dot(a, c), std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, vec3::dot(a, d), std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, vec3::dot(a, e), std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, vec3::dot(a, f), std::numeric_limits<float>::epsilon());
	}

	TEST_METHOD(CrossTest)
	{
		vec3 a(1.0f, 0.0f, 0.0f);
		vec3 b(-1.0f, 0.0f, 0.0f);
		vec3 c(0.0f, 1.0f, 0.0f);
		vec3 d(0.0f, -1.0f, 0.0f);
		vec3 e(0.0f, 0.0, 1.0f);
		vec3 f(0.0f, 0.0f, -1.0f);
		vec3 g;

		Assert::AreEqual<vec3>(g, vec3::cross(a, a));
		Assert::AreEqual<vec3>(g, vec3::cross(a, b));
		Assert::AreEqual<vec3>(e, vec3::cross(a, c));
		Assert::AreEqual<vec3>(f, vec3::cross(a, d));
		Assert::AreEqual<vec3>(d, vec3::cross(a, e));
		Assert::AreEqual<vec3>(c, vec3::cross(a, f));
		Assert::AreEqual<vec3>(g, vec3::cross(a, g));
	}

	TEST_METHOD(OperatorTest)
	{
		vec3 a;
		Assert::AreEqual(0.0f, a[0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, a[1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, a[2], std::numeric_limits<float>::epsilon());

		a[0] = 1.0f;
		a[1] = 2.0f;
		a[2] = 3.0f;
		Assert::AreEqual(1.0f, a[0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(2.0f, a[1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(3.0f, a[2], std::numeric_limits<float>::epsilon());

		a *= 4.0f;
		Assert::AreEqual(4.0f, a[0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(8.0f, a[1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(12.0f, a[2], std::numeric_limits<float>::epsilon());

		a /= 2.0f;
		Assert::AreEqual(2.0f, a[0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(4.0f, a[1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(6.0f, a[2], std::numeric_limits<float>::epsilon());

		a += a;
		Assert::AreEqual(4.0f, a[0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(8.0f, a[1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(12.0f, a[2], std::numeric_limits<float>::epsilon());

		a += a += a;
		Assert::AreEqual(16.0f, a[0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(32.0f, a[1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(48.0f, a[2], std::numeric_limits<float>::epsilon());

		vec3 b(8.0f, 4.0f, 1.0f);
		a -= b;
		Assert::AreEqual(8.0f, a[0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(28.0f, a[1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(47.0f, a[2], std::numeric_limits<float>::epsilon());

		Assert::IsTrue(b == b);
		Assert::IsFalse(a == b);

		a = b * 2.0f;
		Assert::AreEqual<vec3>(vec3(16.0f, 8.0f, 2.0f), a);

		a = a / 4.0f;
		Assert::AreEqual<vec3>(vec3(4.0f, 2.0f, 2.0f / 4.0f), a);

		a = 2.0f * a;
		Assert::AreEqual<vec3>(vec3(8.0f, 4.0f, 1.0f), a);

		Assert::AreEqual<vec3>(vec3(-8.0f, -4.0f, -1.0f), -a);

		a = b + b;
		Assert::AreEqual<vec3>(vec3(16.0f, 8.0f, 2.0f), a);

		a = b - b;
		Assert::AreEqual<vec3>(vec3(0.0f, 0.0f, 0.0f), a);

		vec3 c(2.0f, 4.0f, 8.0f);
		float m = vec3::normalize(vec3::cross(a + 2.0f * (b - (c * 4.0f) / 2.0f), b)).magnitude();
		Assert::AreEqual(1.0f, m, std::numeric_limits<float>::epsilon());
	}
};