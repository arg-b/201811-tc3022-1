#include "stdafx.h"
#include "CppUnitTest.h"
#include <cmath>
#include <limits>
#include <locale>
#include <codecvt>
#include "../201811 TC3022-1/mat3.h"
#include "../201811 TC3022-1/vec3.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Microsoft {
	namespace VisualStudio {
		namespace CppUnitTestFramework {
			template<>
			static std::wstring ToString<mat3>(const mat3& m) {
				std::stringstream ss;
				ss << m;
				std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
				std::wstring wide = converter.from_bytes(ss.rdbuf()->str());
				return wide;
			}
			template<>
			static std::wstring ToString<vec3>(const vec3& v) {
				return L"(" + std::to_wstring(v.x) + L", " + std::to_wstring(v.y) + L", " + std::to_wstring(v.z) + L")";
			}
		}
	}
}

TEST_CLASS(Mat3Test)
{
public:

	TEST_METHOD(ConstructorTest)
	{
		mat3 a;
		Assert::AreEqual(0.0f, a[0][0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, a[0][1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, a[0][2], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, a[1][0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, a[1][1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, a[1][2], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, a[2][0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, a[2][1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, a[2][2], std::numeric_limits<float>::epsilon());

		mat3 b(1.0f);
		Assert::AreEqual(1.0f, b[0][0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, b[0][1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, b[0][2], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, b[1][0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(1.0f, b[1][1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, b[1][2], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, b[2][0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, b[2][1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(1.0f, b[2][2], std::numeric_limits<float>::epsilon());

		vec3 col1(0.0f, 1.0f, 2.0f);
		vec3 col2(3.0f, 4.0f, 5.0f);
		vec3 col3(6.0f, 7.0f, 8.0f);
		mat3 c(col1, col2, col3);
		Assert::AreEqual(0.0f, c[0][0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(1.0f, c[0][1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(2.0f, c[0][2], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(3.0f, c[1][0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(4.0f, c[1][1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(5.0f, c[1][2], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(6.0f, c[2][0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(7.0f, c[2][1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(8.0f, c[2][2], std::numeric_limits<float>::epsilon());
	}

	TEST_METHOD(IndexTest)
	{
		vec3 col1(1.0f, 2.0f, 3.0f);
		vec3 col2(4.0f, 5.0f, 6.0f);
		vec3 col3(7.0f, 8.0f, 9.0f);
		mat3 a(col1, col2, col3);
		Assert::AreEqual(1.0f, a[0][0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(2.0f, a[0][1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(3.0f, a[0][2], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(4.0f, a[1][0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(5.0f, a[1][1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(6.0f, a[1][2], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(7.0f, a[2][0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(8.0f, a[2][1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(9.0f, a[2][2], std::numeric_limits<float>::epsilon());

		a[0][0] = 1.0f + 22.0f;
		a[0][1] = 22.0f - 3.0f;
		a[0][2] = 18.0f;
		a[1][0] -= 2.0f;
		a[1][1] += 11.0f;
		a[1][2] = 6.0f / 3.0f;
		a[2][0] = 7.0f * 3.0f;
		a[2][1] *= 2.0f;
		a[2][2] /= 3.0f;

		Assert::AreEqual(1.0f + 22.0f, a[0][0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(22.0f - 3.0f, a[0][1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(18.0f, a[0][2], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(4.0f - 2.0f, a[1][0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(5.0f + 11.0f, a[1][1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(6.0f / 3.0f, a[1][2], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(7.0f * 3.0f, a[2][0], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(8.0f * 2.0f, a[2][1], std::numeric_limits<float>::epsilon());
		Assert::AreEqual(9.0f / 3.0f, a[2][2], std::numeric_limits<float>::epsilon());
	}

	TEST_METHOD(Mat3xVec3)
	{
		vec3 v1(3.0f, 6.0f, 4.0f);
		mat3 m(1.0f);
		Assert::AreEqual<vec3>(v1, m * v1);

		m[0][0] *= 3.0f;
		m[1][1] *= 3.0f;
		m[2][2] *= 3.0f;
		Assert::AreEqual<vec3>(v1 * 3.0f, m * v1);

		vec3 v2(2.0f, -40.0f, 1.0f);
		m[2][0] = v2.x;
		m[2][1] = v2.y;
		m[2][2] = v2.z;
		Assert::AreEqual<vec3>(vec3(17.0f, -142.0f, 4.0f), m * v1);

		vec3 v3(-3.0f, 0.0f, 8.0f);
		m[1][0] = -2.0f;
		m[0][1] = -3.0f;
		m[0][2] = -7.0f;
		Assert::AreEqual<vec3>(vec3(7.0f, -311.0f, 29.0f), m * v3);

		vec3 v4;
		Assert::AreEqual<vec3>(vec3(), m * v4);

		mat3 m2;
		Assert::AreEqual<vec3>(v4, m2 * v2);

		v4[0] = 1.0f;
		v4[1] = 0.0f;
		v4[2] = 1.0f;

		m2[0][0] = cos(3.14159f);	m2[1][0] = -sin(3.14159f);	m2[2][0] = 0.0f;
		m2[0][1] = sin(3.14159f);	m2[1][1] = cos(3.14159f);	m2[2][1] = 0.0f;
		m2[0][2] = 0.0f;			m2[1][2] = 0.0f;			m2[2][2] = 1.0f;
		Assert::AreEqual(-1.0f, (m2 * v4).x, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, (m2 * v4).y, 0.00001f);
		Assert::AreEqual(1.0f, (m2 * v4).z, std::numeric_limits<float>::epsilon());
	}

	TEST_METHOD(Mat3xMat3)
	{
		mat3 m1;
		mat3 m2(1.0f);

		Assert::AreEqual<mat3>(m1, m1 * m2);
		Assert::AreNotEqual<mat3>(m2, m2 * m1);

		m1[0][0] = 1.0f;	m1[1][0] = 2.0f;	m1[2][0] = 3.0f;
		m1[0][1] = 4.0f;	m1[1][1] = 5.0f;	m1[2][1] = 6.0f;
		m1[0][2] = 7.0f;	m1[1][2] = 8.0f;	m1[2][2] = 9.0f;

		m2[0][0] = -1.0f;	m2[1][0] = -2.0f;	m2[2][0] = -3.0f;
		m2[0][1] = -4.0f;	m2[1][1] = -5.0f;	m2[2][1] = -6.0f;
		m2[0][2] = -7.0f;	m2[1][2] = -8.0f;	m2[2][2] = -9.0f;

		mat3 r;
		r[0][0] = -30.0f;	r[1][0] = -36.0f;	r[2][0] = -42.0f;
		r[0][1] = -66.0f;	r[1][1] = -81.0f;	r[2][1] = -96.0f;
		r[0][2] = -102.0f;	r[1][2] = -126.0f;	r[2][2] = -150.0f;
		Assert::AreEqual(r, m1 * m2);

		m1[0][0] = 1.0f;	m1[1][0] = 2.0f;	m1[2][0] = 3.0f;
		m1[0][1] = 4.0f;	m1[1][1] = 5.0f;	m1[2][1] = 6.0f;
		m1[0][2] = 7.0f;	m1[1][2] = 8.0f;	m1[2][2] = 9.0f;

		m2[0][0] = 10.0f;	m2[1][0] = 11.0f;	m2[2][0] = 12.0f;
		m2[0][1] = 13.0f;	m2[1][1] = 14.0f;	m2[2][1] = 15.0f;
		m2[0][2] = 16.0f;	m2[1][2] = 17.0f;	m2[2][2] = 18.0f;

		r[0][0] = 84.0f;	r[1][0] = 90.0f;	r[2][0] = 96.0f;
		r[0][1] = 201.0f;	r[1][1] = 216.0f;	r[2][1] = 231.0f;
		r[0][2] = 318.0f;	r[1][2] = 342.0f;	r[2][2] = 366.0f;
		Assert::AreEqual(r, m1 * m2);
		Assert::AreNotEqual(r, m2 * m1);

		r[0][0] = 138.0f;	r[1][0] = 171.0f;	r[2][0] = 204.0f;
		r[0][1] = 174.0f;	r[1][1] = 216.0f;	r[2][1] = 258.0f;
		r[0][2] = 210.0f;	r[1][2] = 261.0f;	r[2][2] = 312.0f;
		Assert::AreEqual(r, m2 * m1);

		vec3 v1(2.0f, 0.0f, 1.0f);

		float pi = 3.1415926535897932384626433832795f;
		m1[0][0] = cos(pi);		m1[1][0] = -sin(pi);	m1[2][0] = 0.0f;
		m1[0][1] = sin(pi);		m1[1][1] = cos(pi);		m1[2][1] = 0.0f;
		m1[0][2] = 0.0f;		m1[1][2] = 0.0f;		m1[2][2] = 1.0f;

		m2[0][0] = cos(pi);		m2[1][0] = -sin(pi);	m2[2][0] = 0.0f;
		m2[0][1] = sin(pi);		m2[1][1] = cos(pi);		m2[2][1] = 0.0f;
		m2[0][2] = 0.0f;		m2[1][2] = 0.0f;		m2[2][2] = 1.0f;
		Assert::AreEqual(2.0f, (m1 * m2 * v1).x, std::numeric_limits<float>::epsilon());
		Assert::AreEqual(0.0f, (m1 * m2 * v1).y, 0.000001f);
		Assert::AreEqual(1.0f, (m1 * m2 * v1).z, std::numeric_limits<float>::epsilon());
	}
};