#pragma once

#include "scene.h"

#include "mesh.h"
#include "shader_program.h"

class scene_exam : public scene
{
public:
	scene_exam() {}
	~scene_exam() {}

	void init();
	void awake();
	void sleep() { }
	void reset() { }
	void mainLoop();
	void resize(int width, int height);
	void normalKeysDown(unsigned char key) { }
	void normalKeysUp(unsigned char key) { }
	void specialKeys(int key) { }
	void passiveMotion(int x, int y) { }

private:
	mesh circleMesh;
	shader_program circleShader;
};