#include "scene_sinewave.h"

#include <vector>

#include "cgmath.h"
#include "time.h"

void scene_sinewave::init()
{
	std::vector<vec3> positions;
	std::vector<vec2> texCoords;

	int width = 20;

	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < width; j++)
		{
			positions.push_back(vec3(i, 0.0f, j));
			texCoords.push_back(vec2((float)i / (float)(width - 1), (float)j / (float)(width - 1)));
		}
	}

	std::vector<unsigned int> indices;

	for (int i = 0; i < width - 1; i++)
	{
		for (int j = 0; j < width - 1; j++)
		{
			indices.push_back(i * width + j + 1);
			indices.push_back(i * width + j + width + 1);
			indices.push_back(i * width + j + width);

			indices.push_back(i * width + j + 1);
			indices.push_back(i * width + j + width);
			indices.push_back(i * width + j);
		}
	}

	planeMesh.create(positions.size());
	planeMesh.setAttributePosition(positions, 0);
	planeMesh.setAttributeTexCoord(texCoords, 1);
	planeMesh.setIndices(indices);

	planeShader.create();
	planeShader.attachShader("SineWave.vert", GL_VERTEX_SHADER);
	planeShader.attachShader("SineWave.frag", GL_FRAGMENT_SHADER);
	planeShader.setAttribute(0, "VertexPosition");
	planeShader.setAttribute(1, "VertexTexCoord");
	planeShader.link();

	planeShader.activate();
	planeShader.setUniformi("DiffuseTexture", 0);
	planeShader.deactivate();

	planeTexture.loadTexture("crate.png");

	sceneCamera.setPosition(9.5f, 20.0f, 30.0f);
	sceneCamera.setRotation(-45.0f, 0.0f, 0.0f);
}

void scene_sinewave::mainLoop()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	planeShader.activate();
	planeShader.setUniformMatrix("mvpMatrix", sceneCamera.getProjectionMatrix() * sceneCamera.getViewMatrix() * planeTransform.getModelMatrix());
	planeShader.setUniformf("time", time::elapsedTime().count());

	glActiveTexture(GL_TEXTURE0);
	planeTexture.bind();

	planeMesh.draw(GL_TRIANGLES);

	glActiveTexture(GL_TEXTURE0);
	planeTexture.unbind();

	planeShader.deactivate();
}

void scene_sinewave::resize(int width, int height)
{
	glViewport(0, 0, width, height);
	sceneCamera.setPerspective(1.0f, 1000.0f, 60.0f, (float)width / (float)height);
}
