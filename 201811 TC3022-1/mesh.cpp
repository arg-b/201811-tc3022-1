#include "mesh.h"

mesh::mesh()
{
	// Inicializamos todas las variables a su valor por default
	_vertexArrayObject = 0;
	_positionsVertexBufferObject = 0;
	_colorsVertexBufferObject = 0;
	_normalsVertexBufferObject = 0;
	_texCoordsVertexBufferObject = 0;
	_indexBufferObject = 0;
	_vertexCount = 0;
	_indexCount = 0;
}

mesh::~mesh()
{
	// Solo borramos la memoria si _vertexArrayObject es diferente de 0
	if (_vertexArrayObject)
		glDeleteVertexArrays(1, &_vertexArrayObject);

	// Solo borramos la memoria si _positionsVertexBufferObject es diferente de 0.
	// Es decir, solo cuando alguien creo un buffer con el atributo de posiciones.
	if (_positionsVertexBufferObject)
		glDeleteBuffers(1, &_positionsVertexBufferObject);

	// Solo borramos la memoria si _colorsVertexBufferObject es diferente de 0.
	// Es decir, solo cuando alguien creo un buffer con el atributo de colores.
	if (_colorsVertexBufferObject)
		glDeleteBuffers(1, &_colorsVertexBufferObject);

	if (_normalsVertexBufferObject)
		glDeleteBuffers(1, &_normalsVertexBufferObject);

	if (_texCoordsVertexBufferObject)
		glDeleteBuffers(1, &_texCoordsVertexBufferObject);

	if (_indexBufferObject)
		glDeleteBuffers(1, &_indexBufferObject);
}

void mesh::create(int vertexCount)
{
	// Inicializamos el conteo de vertices
	_vertexCount = vertexCount;
	// Creamos la memoria necesaria para el manager de vertices (Vertex Array Object)
	// En este momento _vertexArrayObject deja de tener un valor de 0.
	// El valor depende completamente de OpenGL, cuando queramos utilizar este manager,
	// utilizaremos este identificador.
	glGenVertexArrays(1, &_vertexArrayObject);
}

void mesh::draw(GLenum primitive)
{
	// Activamos el manager de vertices (VAO). Es necesario para utilizar el comando de dibujado.
	glBindVertexArray(_vertexArrayObject);
	// Ejecutamos el comando de dibujado de OpenGl. Utilizamos el parametro como primitiva.
	// 0 -> indice del primer vertice a dibujar.
	// _vertexCount -> cuantos vertices queremos dibujar.
	if (_indexCount > 0)
		glDrawElements(primitive,
			_indexCount,
			GL_UNSIGNED_INT, nullptr);
	else
		glDrawArrays(primitive, 0, _vertexCount);
	// Desactivamos el manager de vertices (VAO).
	glBindVertexArray(0);
}

void mesh::setAttributePosition(std::vector<vec2> positions, GLuint locationIndex)
{
	// Si el numero de elementos en el atributo no corresponde al numero de vertices en la malla poligonal,
	// no creamos el buffer para prevenir errores en la tarjeta de video al leer posiciones de memoria que no
	// corresponden.
	if (positions.size() != _vertexCount)
		return;

	// Si el buffer ya existia (quiza se quieren cambiar los datos), lo borramos primero antes de volverlo a crear
	if (_positionsVertexBufferObject)
		glDeleteBuffers(1, &_positionsVertexBufferObject);

	// Activamos el manager. Todos los buffers creados a continuacion, se asocian automaticamente.
	glBindVertexArray(_vertexArrayObject);

	// Creamos 1 buffer y lo guardamos en la variable _positionsVertexBufferObject
	glGenBuffers(1, &_positionsVertexBufferObject);
	// Activamos el buffer como un atributo (para eso usamos el target -> GL_ARRAY_BUFFER)
	glBindBuffer(GL_ARRAY_BUFFER, _positionsVertexBufferObject);
	// Cargar los datos en el buffer y mandarlos a la tarjeta de video
	// Primer parametro es el target (GL_ARRAY_BUFFER para indicar que se trata de un atributo).
	// Segundo parametro es el tamanio en bytes de la lista. Calculamos el tamanio de un elemento y lo multiplicamos
	// por el total de elementos en la lista.
	// Tercer parametro es una referencia al primer elemento en la lista. El metodo data (de vector) nos da una
	// referencia al primer elemento en la lista.
	// GL_STATIC_DRAW indica que vamos a especificar estos vertices 1 vez y los vamos a utilizar multiples veces.
	// OpenGL utiliza esta informacion para decidir donde almacena los datos (el tipo de memoria mas optima).
	glBufferData(GL_ARRAY_BUFFER, sizeof(vec2) * positions.size(), positions.data(), GL_STATIC_DRAW);
	// Activamos el atributo con indice locationIndex
	glEnableVertexAttribArray(locationIndex);
	// Configuramos OpenGL para que pueda interpretar los datos que mandamos. 
	// En este caso, indicamos que cada dato tiene 2 componentes (X, Y) y que cada componente es de tipo float.
	glVertexAttribPointer(locationIndex, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
	// Desactivamos el buffer de posiciones
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Desactivamos el manager
	glBindVertexArray(0);
}

void mesh::setAttributePosition(std::vector<vec3> positions, GLuint locationIndex)
{
	if (positions.size() != _vertexCount)
		return;

	if (_positionsVertexBufferObject)
		glDeleteBuffers(1, &_positionsVertexBufferObject);

	glBindVertexArray(_vertexArrayObject);

	glGenBuffers(1, &_positionsVertexBufferObject);
	glBindBuffer(GL_ARRAY_BUFFER, _positionsVertexBufferObject);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vec3) * positions.size(), positions.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(locationIndex);
	glVertexAttribPointer(locationIndex, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);
}

void mesh::setAttributeColor(std::vector<vec3> colors, GLuint locationIndex)
{
	if (colors.size() != _vertexCount)
		return;

	if (_colorsVertexBufferObject)
		glDeleteBuffers(1, &_colorsVertexBufferObject);

	glBindVertexArray(_vertexArrayObject);

	glGenBuffers(1, &_colorsVertexBufferObject);
	glBindBuffer(GL_ARRAY_BUFFER, _colorsVertexBufferObject);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vec3) * colors.size(), colors.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(locationIndex);
	glVertexAttribPointer(locationIndex, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);
}

void mesh::setAttributeNormal(std::vector<vec3> normals, GLuint locationIndex)
{
	if (normals.size() != _vertexCount)
		return;

	if (_normalsVertexBufferObject)
		glDeleteBuffers(1, &_normalsVertexBufferObject);

	glBindVertexArray(_vertexArrayObject);

	glGenBuffers(1, &_normalsVertexBufferObject);
	glBindBuffer(GL_ARRAY_BUFFER, _normalsVertexBufferObject);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vec3) * normals.size(), normals.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(locationIndex);
	glVertexAttribPointer(locationIndex, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);
}

void mesh::setAttributeTexCoord(std::vector<vec2> texCoords, GLuint locationIndex)
{
	if (texCoords.size() != _vertexCount)
		return;

	if (_texCoordsVertexBufferObject)
		glDeleteBuffers(1, &_texCoordsVertexBufferObject);

	glBindVertexArray(_vertexArrayObject);

	glGenBuffers(1, &_texCoordsVertexBufferObject);
	glBindBuffer(GL_ARRAY_BUFFER, _texCoordsVertexBufferObject);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vec2) * texCoords.size(), texCoords.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(locationIndex);
	glVertexAttribPointer(locationIndex, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);
}

void mesh::setIndices(std::vector<unsigned int> indices)
{
	if (indices.size() <= 0)
		return;

	_indexCount = indices.size();

	if (_indexBufferObject)
		glDeleteBuffers(1, &_indexBufferObject);

	glBindVertexArray(_vertexArrayObject);

	glGenBuffers(1, &_indexBufferObject);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _indexBufferObject);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,
		sizeof(unsigned int) * indices.size(),
		indices.data(),
		GL_STATIC_DRAW);

	glBindVertexArray(0);
}

const int mesh::getVertexCount() const
{
	// Regresamos el numero de vertices en esta malla poligonal.
	return _vertexCount;
}