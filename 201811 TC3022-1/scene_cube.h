#pragma once

#include "camera.h"
#include "mesh.h"
#include "scene.h"
#include "shader_program.h"
#include "transform.h"

class scene_cube : public scene
{
public:
	scene_cube();
	~scene_cube() {}

	void init();
	void awake();
	void sleep() { }
	void reset() { }
	void mainLoop();
	void resize(int width, int height);
	void normalKeysDown(unsigned char key);
	void normalKeysUp(unsigned char key);
	void specialKeys(int key);
	void passiveMotion(int x, int y) { }

private:
	camera sceneCamera;
	mesh cubeMesh;
	shader_program cubeShader;
	transform cubeTransform;

	bool wPressed;
	bool dPressed;
	bool sPressed;
	bool aPressed;
};