#pragma once

#include "camera.h"
#include "mesh.h"
#include "scene.h"
#include "shader_program.h"
#include "transform.h"

class scene_mouserotation : public scene
{
public:
	scene_mouserotation();
	~scene_mouserotation() { }

	void init();
	void awake();
	void sleep() { }
	void reset() { }
	void mainLoop();
	void resize(int width, int height);
	void normalKeysDown(unsigned char key) { }
	void normalKeysUp(unsigned char key) { }
	void specialKeys(int key) { }
	void passiveMotion(int x, int y);

private:
	camera sceneCamera;
	mesh cubeMesh;
	shader_program cubeShader;
	transform cubeTransform;

	vec2 screenCenter;
	float smoothFactor;
};