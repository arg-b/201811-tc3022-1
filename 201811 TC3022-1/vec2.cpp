#include "vec2.h"

vec2::vec2()
{
	x = y = 0.0f;
}

vec2::vec2(float x, float y)
{
	this->x = x;
	this->y = y;
}

float& vec2::operator[](int i)
{
	return (&x)[i];
}

const float& vec2::operator[](int i) const
{
	return (&x)[i];
}

vec2& vec2::operator*=(float s)
{
	x *= s;
	y *= s;
	return *this;
}

vec2& vec2::operator/=(float s)
{
	s = 1.0f / s;
	x *= s;
	y *= s;
	return *this;
}

vec2& vec2::operator+=(const vec2& v)
{
	x += v.x;
	y += v.y;
	return *this;
}

vec2& vec2::operator-=(const vec2& v)
{
	x -= v.x;
	y -= v.y;
	return *this;
}

bool vec2::operator==(const vec2& v) const
{
	return x == v.x && y == v.y;
}

float vec2::magnitude() const
{
	return sqrt(x * x + y * y);
}

void vec2::normalize()
{
	float m = magnitude();
	x /= m;
	y /= m;
}

float vec2::magnitude(const vec2& v)
{
	return v.magnitude();
}

vec2 vec2::normalize(const vec2& v)
{
	return v / v.magnitude();
}

float vec2::dot(const vec2& a, const vec2& b)
{
	return a.x * b.x + a.y * b.y;
}
