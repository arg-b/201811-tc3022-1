#include "scene_exam.h"

#include "cgmath.h"
#include <vector>

void scene_exam::init()
{
	std::vector<vec2> positions;
	std::vector<vec3> colors;
	std::vector<unsigned int> indices;

	positions.push_back(vec2(0.0f, 0.0f));
	colors.push_back(vec3(1.0f, 1.0f, 1.0f));

	for (int i = 0; i < 360; i++) {

		float x = cos(cgmath::radians(i));
		float y = sin(cgmath::radians(i));

		positions.push_back(vec2(x, y));
		colors.push_back(vec3(x, y, x * y));

		indices.push_back(i);
	}

	indices.push_back(360);
	indices.push_back(1);

	circleMesh.create(361);
	circleMesh.setAttributePosition(positions, 0);
	circleMesh.setAttributeColor(colors, 1);
	circleMesh.setIndices(indices);

	circleShader.create();
	circleShader.attachShader("DiscardCenter.vert", GL_VERTEX_SHADER);
	circleShader.attachShader("DiscardCenter.frag", GL_FRAGMENT_SHADER);
	circleShader.setAttribute(0, "VertexPosition");
	circleShader.setAttribute(1, "VertexColor");
	circleShader.link();
}

void scene_exam::awake()
{
	glClearColor(1.0f, 1.0f, 0.5f, 1.0f);
}

void scene_exam::mainLoop()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	circleShader.activate();
	circleShader.setUniformMatrix("mvpMatrix", mat4(1.0f));
	circleShader.setUniformf("iResolution", 400.0f, 400.0f);
	circleMesh.draw(GL_TRIANGLE_FAN);
	circleShader.deactivate();
}

void scene_exam::resize(int width, int height)
{
	glViewport(0, 0, width, height);
}
