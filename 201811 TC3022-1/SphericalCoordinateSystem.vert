#version 330 core

out vec3 InterpolatedColor;

uniform float time;

void main()
{
  float angleOffset = 5.0;
  float theta = radians(mod(gl_VertexID * angleOffset, 180.0));
  float fi = radians(mod(floor(gl_VertexID / (180.0 / angleOffset)) * angleOffset, 360.0));
  
  float x = sin(theta) * cos(fi);
  float y = sin(theta) * sin(fi);
  float z = cos(theta);
  
  vec3 pos = vec3(x, y, z);
  
  mat4 rotx = mat4(1.0);
  rotx[1][1] = cos(time);
  rotx[2][1] = -sin(time);
  rotx[1][2] = sin(time);
  rotx[2][2] = cos(time);
  
  mat4 rotz = mat4(1.0);
  rotz[0][0] = cos(time);
  rotz[1][0] = -sin(time);
  rotz[0][1] = sin(time);
  rotz[1][1] = cos(time);
  
  
  gl_Position = rotz * rotx * vec4(pos, 1.0);
  gl_PointSize = 2.0;
  InterpolatedColor = vec3(1.0, 0.0, 0.0);
}