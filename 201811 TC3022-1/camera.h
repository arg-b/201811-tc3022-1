#pragma once

#include "cgmath.h"
#include "transform.h"

class camera
{
public:
	camera();

	void setPerspective(float near, float far, float fov, float aspect);

	void setPosition(float x, float y, float z);
	void setRotation(float x, float y, float z);

	void moveForward(float delta);
	void moveRight(float delta);
	void moveUp(float delta);

	void yaw(float degrees);

	mat4 getProjectionMatrix() const;
	mat4 getViewMatrix() const;
private:
	mat4 _projectionMatrix;
	mat4 _viewMatrix;

	transform _cameraTransform;
};