#pragma once

#include "cgmath.h"

class transform
{
public:
	// Iniciamos la matriz de modelo como
	// la matriz identidad
	transform();

	// Cambiar la posicion actual del modelo a la
	// especificada por las componentes (x, y, z)
	void setPosition(float x, float y, float z);
	// Cambia la rotacion actual del modelo a la
	// especificada por las componentes (x, y, z).
	// Se reciben angulos en sistema Euleriano
	void setRotation(float x, float y, float z);
	// Cambiar la escala actual del modelo a la 
	// escpecificada por las componentes (x, y, z)
	void setScale(float x, float y, float z);

	void moveForward(float delta);
	void moveRight(float delta);
	void moveUp(float delta);

	void yaw(float degrees);

	// Regresa la posicion del objeto
	vec3 getPosition() const;
	// Regresa la rotacion del objeto
	quat getRotation() const;
	// Regresa la escala del objeto
	vec3 getScale() const;
	// Regresa la matriz de modelo del objeto
	mat4 getModelMatrix() const;

	void setModelMatrix(const mat4& modelMatrix);

private:
	// La posicion del objeto con este transform
	vec3 _position;

	// La rotacion del objeto con este transform
	quat _rotation;

	// La escala del objeto con este transform
	vec3 _scale;

	// El resultado de combinar la posicion,
	// rotacion y escala del objeto
	mat4 _modelMatrix;

	// Actualizar la matriz de modelo cuando el objeto
	// ha sido trasladado
	void updateModelMatrixTranslation();
	// Actualizar la matriz de modelo cuando el objeto
	// ha sido escalado.
	// 1. igualar la matriz de modelo a la identidad
	// 2. Actualizar la matriz para considerar traslacion
	// 3. Multiplicar la columna 0 por escala x
	// 3. Multiplicar la columna 1 por escala y
	// 3. Multiplicar la columna 2 por escala z
	void updateModelMatrixRotationScale();
};