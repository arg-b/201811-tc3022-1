#version 330 core

in vec3 InterpolatedColor;

out vec4 FragColor;

uniform vec2 iResolution;

void main()
{
	vec2 p = gl_FragCoord.xy / iResolution;
	vec2 center = vec2(0.5f, 0.5f);
	vec2 q = p - center;

	if (length(q) < 0.25)
		discard;

	FragColor = vec4(InterpolatedColor, 1.0f);
}