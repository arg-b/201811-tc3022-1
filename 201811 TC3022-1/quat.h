#pragma once

#include "cgmath.h"

class quat
{
public:
	float x, y, z, w;

	quat();
	quat(float x, float y, float z, float w);
	quat(const vec3& eulerAngles);

	mat4 getRotationMatrix() const;

	void fromMatrix(const mat4& matrix);
};

inline quat operator*(const quat& q1, const quat& q2)
{
	float x = q1.w * q2.x + q1.x * q2.w + q1.y * q2.z - q1.z * q2.y;
	float y = q1.w * q2.y - q1.x * q2.z + q1.y * q2.w + q1.z * q2.x;
	float z = q1.w * q2.z + q1.x * q2.y - q1.y * q2.x + q1.z * q2.w;
	float w = q1.w * q2.w - q1.x * q2.x - q1.y * q2.y - q1.z * q2.z;

	return quat(x, y, z, w);
}