#include "vec3.h"

vec3::vec3()
{
	// Asignamos los valores por default
	x = y = z = 0.0f;
}

vec3::vec3(float x, float y, float z)
{
	// Asignamos los valores con los que estan construyendo este objeto
	this->x = x;
	this->y = y;
	this->z = z;
}

float vec3::magnitude() const
{
	// Raiz cuadrada de la sumatoria del cuadrado de las componentes
	return sqrt(x * x + y * y + z * z);
}

void vec3::normalize()
{
	float m = magnitude();
	x /= m;
	y /= m;
	z /= m;
}

float vec3::magnitude(const vec3& v)
{
	// Utilizamos la version magnitude del objeto v por facilidad
	return v.magnitude();
}

vec3 vec3::normalize(const vec3& v)
{
	return v / v.magnitude();
}

float vec3::dot(const vec3& a, const vec3& b)
{
	return a.x * b.x + a.y * b.y + a.z * b.z;
}

vec3 vec3::cross(const vec3& a, const vec3& b)
{
	return vec3(a.y * b.z - a.z * b.y, a.z * b.x - a.x * b.z, a.x * b.y - a.y * b.x);
}

float& vec3::operator[](int i)
{
	// Permite obtener y asignar un valor (x, y, z) utilizando el operador []
	return (&x)[i];
}

const float& vec3::operator[](int i) const
{
	// Permite unicamente obtener un valor (x, y, z) utilizando el operador []
	return (&x)[i];
}

vec3& vec3::operator*=(float s)
{
	x *= s;
	y *= s;
	z *= s;
	return *this;
}

vec3& vec3::operator/=(float s)
{
	s = 1.0f / s;
	x *= s;
	y *= s;
	z *= s;
	return *this;
}

vec3& vec3::operator+=(const vec3& v)
{
	x += v.x;
	y += v.y;
	z += v.z;
	return *this;
}

vec3& vec3::operator-=(const vec3& v)
{
	x -= v.x;
	y -= v.y;
	z -= v.z;
	return *this;
}

bool vec3::operator==(const vec3& v) const
{
	return x == v.x && y == v.y && z == v.z;
}
