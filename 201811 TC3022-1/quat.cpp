#include "quat.h"

#include <iostream>

quat::quat()
{
	x = y = z = w = 0;
}

quat::quat(float x, float y, float z, float w)
{
	this->x = x;
	this->y = y;
	this->z = z;
	this->w = w;
}

quat::quat(const vec3& eulerAngles)
{
	float x = cgmath::radians(eulerAngles.x * 0.5f);
	float y = cgmath::radians(eulerAngles.y * 0.5f);
	float z = cgmath::radians(eulerAngles.z * 0.5f);

	vec3 c(cos(x), cos(y), cos(z));
	vec3 s(sin(x), sin(y), sin(z));

	this->w = c.x * c.y * c.z + s.x * s.y * s.z;
	this->x = s.x * c.y * c.z - c.x * s.y * s.z;
	this->y = c.x * s.y * c.z + s.x * c.y * s.z;
	this->z = c.x * c.y * s.z - s.x * s.y * c.z;
}

mat4 quat::getRotationMatrix() const
{
	float x2 = x * x;
	float y2 = y * y;
	float z2 = z * z;
	float xy = x * y;
	float xz = x * z;
	float yz = y * z;
	float wx = w * x;
	float wy = w * y;
	float wz = w * z;

	vec4 a(1.0f - 2.0f * (y2 + z2), 2.0f * (xy + wz), 2.0f * (xz - wy), 0.0f);
	vec4 b(2.0f * (xy - wz), 1.0f - 2.0f * (x2 + z2), 2.0f * (yz + wx), 0.0f);
	vec4 c(2.0f * (xz + wy), 2.0f * (yz - wx), 1.0f - 2.0f * (x2 + y2), 0.0f);
	vec4 d(0.0f, 0.0f, 0.0f, 1.0f);

	return mat4(a, b, c, d);
}

void quat::fromMatrix(const mat4& m)
{
	float fourXSquaredMinus1 = m[0][0] - m[1][1] - m[2][2];
	float fourYSquaredMinus1 = m[1][1] - m[0][0] - m[2][2];
	float fourZSquaredMinus1 = m[2][2] - m[0][0] - m[1][1];
	float fourWSquaredMinus1 = m[0][0] + m[1][1] + m[2][2];

	int biggestIndex = 0;
	float fourBiggestSquaredMinus1 = fourWSquaredMinus1;
	if (fourXSquaredMinus1 > fourBiggestSquaredMinus1)
	{
		fourBiggestSquaredMinus1 = fourXSquaredMinus1;
		biggestIndex = 1;
	}
	if (fourYSquaredMinus1 > fourBiggestSquaredMinus1)
	{
		fourBiggestSquaredMinus1 = fourYSquaredMinus1;
		biggestIndex = 2;
	}
	if (fourZSquaredMinus1 > fourBiggestSquaredMinus1)
	{
		fourBiggestSquaredMinus1 = fourZSquaredMinus1;
		biggestIndex = 3;
	}

	float biggestVal = sqrt(fourBiggestSquaredMinus1 + 1.0f) * 0.5f;
	float mult = (0.25f) / biggestVal;

	switch (biggestIndex)
	{
	case 0:
		w = biggestVal;
		x = (m[1][2] - m[2][1]) * mult;
		y = (m[2][0] - m[0][2]) * mult;
		z = (m[0][1] - m[1][0]) * mult;
		break;
	case 1:
		w = (m[1][2] - m[2][1]) * mult;
		x = biggestVal;
		y = (m[0][1] + m[1][0]) * mult;
		z = (m[2][0] + m[0][2]) * mult;
		break;
	case 2:
		w = (m[2][0] - m[0][2]) * mult;
		x = (m[0][1] + m[1][0]) * mult;
		y = biggestVal;
		z = (m[1][2] + m[2][1]) * mult;
		break;
	case 3:
		w = (m[0][1] - m[1][0]) * mult;
		x = (m[2][0] + m[0][2]) * mult;
		y = (m[1][2] + m[2][1]) * mult;
		z = biggestVal;
		break;

	default:
		std::cout << "Quaternion error" << std::endl;
		break;
	}
}
