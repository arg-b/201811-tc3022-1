#pragma once

#include <GL/glew.h>
#include <string>
#include <vector>
#include "cgmath.h"

class tex2d
{
public:
	tex2d();
	~tex2d();

	void loadTexture(const std::string& path);
	void loadTexture(const std::vector<vec3>& pixels, int width, int height);
	template<size_t rows, size_t cols> void loadTexture(vec3(&pixels)[rows][cols]);
	void bind();
	void unbind();

private:
	GLuint _textureId;
};

template<size_t rows, size_t cols>
inline void tex2d::loadTexture(vec3(&pixels)[rows][cols])
{
	glGenTextures(1, &_textureId);
	glBindTexture(GL_TEXTURE_2D, _textureId);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, rows, cols, 0, GL_RGB, GL_FLOAT, &pixels[0][0]);
	glBindTexture(GL_TEXTURE_2D, 0);
}