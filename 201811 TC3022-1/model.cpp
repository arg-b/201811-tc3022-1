#include "model.h"

#include <fstream>
#include <sstream>
#include <iostream>
#include <map>
#include <GL/glew.h>

void model::load(const std::string& path)
{
	Assimp::Importer importer;

	const aiScene* scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_GenNormals | aiProcess_FixInfacingNormals);

	if (!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
	{
		std::cout << "Assimp error: " << importer.GetErrorString() << std::endl;
		return;
	}

	processNode(scene->mRootNode, scene);
}

void model::processNode(aiNode* node, const aiScene* scene)
{
	for (GLuint i = 0; i < node->mNumMeshes; i++)
	{
		aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
		processMesh(mesh, scene);
	}

	for (GLuint i = 0; i < node->mNumChildren; i++)
	{
		processNode(node->mChildren[i], scene);
	}
}

void model::processMesh(aiMesh* aMesh, const aiScene* scene)
{
	std::vector<vec3> positions;
	std::vector<vec3> normals;
	std::vector<vec2> texCoords;
	std::vector<vec3> colors;
	std::vector<unsigned int> indices;

	for (GLuint i = 0; i < aMesh->mNumVertices; i++)
	{
		vec3 position(aMesh->mVertices[i].x, aMesh->mVertices[i].y, aMesh->mVertices[i].z);
		positions.push_back(position);

		vec3 normal(aMesh->mNormals[i].x, aMesh->mNormals[i].y, aMesh->mNormals[i].z);
		normals.push_back(normal);

		vec2 texCoord;
		if (aMesh->mTextureCoords[0])
		{
			texCoord.x = aMesh->mTextureCoords[0][i].x;
			texCoord.y = aMesh->mTextureCoords[0][i].y;
		}
		texCoords.push_back(texCoord);

		vec3 color(1.0f, 1.0f, 1.0f);
		if (aMesh->mColors[0])
		{
			color.x = aMesh->mColors[0][i].r;
			color.y = aMesh->mColors[0][i].g;
			color.z = aMesh->mColors[0][i].b;
		}
		colors.push_back(color);
	}

	for (GLuint i = 0; i < aMesh->mNumFaces; i++)
	{
		aiFace face = aMesh->mFaces[i];
		for (GLuint j = 0; j < face.mNumIndices; j++)
		{
			indices.push_back(face.mIndices[j]);
		}
	}

	if (aMesh->mMaterialIndex >= 0)
	{
		aiMaterial* material = scene->mMaterials[aMesh->mMaterialIndex];
		aiString str;
		material->GetTexture(aiTextureType_DIFFUSE, 0, &str);
		textureList.push_back(std::make_unique<tex2d>());
		textureList.back()->loadTexture(str.C_Str());
	}
	else
	{
		textureList.push_back(nullptr);
	}

	std::unique_ptr<mesh> meshBuffer = std::make_unique<mesh>();
	meshBuffer->create(positions.size());
	meshBuffer->setAttributePosition(positions, 0);
	meshBuffer->setAttributeColor(colors, 1);
	meshBuffer->setAttributeNormal(normals, 2);
	meshBuffer->setAttributeTexCoord(texCoords, 3);
	meshBuffer->setIndices(indices);
	meshList.push_back(std::move(meshBuffer));
}

void model::draw()
{
	for (int i = 0; i < meshList.size(); i++)
	{
		if (textureList[i])
		{
			glActiveTexture(GL_TEXTURE0);
			textureList[i]->bind();
		}

		meshList[i]->draw(GL_TRIANGLES);

		if (textureList[i])
		{
			glActiveTexture(GL_TEXTURE0);
			textureList[i]->unbind();
		}
	}
}