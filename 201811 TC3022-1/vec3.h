#pragma once
#include <string>

// El encabezado muestra la descripcion de nuestra clase.
// Por lo general es una transcripcion del diagrama UML.
class vec3
{
// Definimos un bloque publico. Todos los atributos y metodos definidos
// a continuacion son publicos.
public:
	// Atributos
	float x, y, z;

	// Constructor por default
	vec3();
	// Constructor personalizado que inicializa los valores x, y, z
	vec3(float x, float y, float z);

	// Calcular la magnitud de este objeto vec3 (no modifica x, y, z)
	float magnitude() const;

	void normalize();

	// Calcula la magnitud de v. Al ser estatico no necesitamos un objeto para utilizar este metodo.
	static float magnitude(const vec3& v);

	static vec3 normalize(const vec3& v);

	// Permite obtener y asignar un valor (x, y, z) utilizando el operador [] (util para modificar)
	float& operator[](int i);
	// Permite unicamente obtener un valor (x, y, z) utilizando el operador [] (util para imprimir, por ejemplo)
	const float& operator[](int i) const;

	static float dot(const vec3& a, const vec3& b);
	static vec3 cross(const vec3& a, const vec3& b);

	vec3& operator*=(float s);
	vec3& operator/=(float s);
	vec3& operator+=(const vec3& v);
	vec3& operator-=(const vec3& v);
	bool operator==(const vec3& v) const;

	friend std::ostream& operator<<(std::ostream& os, const vec3& v)
	{
		os << "(" << v.x << ", " << v.y << ", " << v.z << ")";
		return os;
	}
};

inline vec3 operator*(const vec3& v, float s)
{
	return vec3(v.x * s, v.y * s, v.z * s);
}

inline vec3 operator*(float s, const vec3& v)
{
	return vec3(v.x * s, v.y * s, v.z * s);
}

inline vec3 operator/(const vec3& v, float s)
{
	s = 1.0f / s;
	return vec3(v.x * s, v.y * s, v.z * s);
}

inline vec3 operator-(const vec3& v)
{
	return vec3(-v.x, -v.y, -v.z);
}

inline vec3 operator+(const vec3& a, const vec3& b)
{
	return vec3(a.x + b.x, a.y + b.y, a.z + b.z);
}

inline vec3 operator-(const vec3& a, const vec3& b)
{
	return vec3(a.x - b.x, a.y - b.y, a.z - b.z);
}