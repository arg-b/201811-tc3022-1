#pragma once

#include <cmath>

#include "vec2.h"
#include "vec3.h"
#include "vec4.h"
#include "mat3.h"
#include "mat4.h"
#include "quat.h"

namespace cgmath
{
	constexpr float pi = 3.14159226535879f;

	const vec3 worldForward(0.0f, 0.0f, 1.0f);
	const vec3 worldUp(0.0f, 1.0f, 0.0f);
	const vec3 worldRight(1.0f, 0.0f, 0.0f);

	inline constexpr float radians(const float& degs)
	{
		return degs * 0.017453292f;
	}

	inline constexpr float degrees(const float& rads)
	{
		return rads * 57.29577951f;
	}
}