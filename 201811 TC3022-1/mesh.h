#pragma once

#include <GL/glew.h>
#include <vector>
#include "cgmath.h"

class mesh
{
public:
	// Constructor de la clase mesh (inicializa valores por default de las variables).
	mesh();
	// Destructor de la clase mesh (borra la memoria utilizada por el objeto).
	~mesh();

	// Inicializa el conteo de vertices y la memoria (utilizando el API de OpenGL)
	// del manager de vertices (Vertex Array Object).
	void create(int vertexCount);
	// Activa el manager de vertices (VAO), ejecuta el comando de dibujado utilizando
	// el parametro como primitiva, y desactiva el manager de vertices (VAO).
	void draw(GLenum primitive);

	// Crea el buffer con el atributo de posiciones (x, y) y lo manda a la tarjeta de video.
	// Activa el atributo con indice locationIndex en la tarjeta de video para uso de las posiciones.
	void setAttributePosition(std::vector<vec2> positions, GLuint locationIndex);
	// Crea el buffer con el atributo de posiciones (x, y, z) y lo manda a la tarjeta de video.
	// Activa el atributo con indice locationIndex en la tarjeta de video para uso de las posiciones.
	void setAttributePosition(std::vector<vec3> positions, GLuint locationIndex);
	// Crea el buffer con el atributo de colores y lo manda a la tarjeta de video.
	// Activa el atributo con indice locationIndex en la tarjeta de video para uso de los colores.
	void setAttributeColor(std::vector<vec3> colors, GLuint locationIndex);
	void setAttributeNormal(std::vector<vec3> normals, GLuint locationIndex);
	void setAttributeTexCoord(std::vector<vec2> texCoords, GLuint locationIndex);
	// Crear el buffer de indices
	void setIndices(std::vector<unsigned int> indices);

	// Obtiene el numero de vertices en esta malla poligonal.
	const int getVertexCount() const;
private:
	// Manager de vertices (Vertex Array Object)
	GLuint _vertexArrayObject;
	// Identificador del VBO de posiciones
	GLuint _positionsVertexBufferObject;
	// Identificador del VBO de colores
	GLuint _colorsVertexBufferObject;
	GLuint _normalsVertexBufferObject;
	GLuint _texCoordsVertexBufferObject;
	// Identificador del buffer de indices
	GLuint _indexBufferObject;
	// Numero de vertices en esta malla poligonal.
	int _vertexCount;
	// Numero de indices que nos han asignado
	int _indexCount;
};