#include "time.h"

hr_clock::time_point time::_startPoint;
hr_clock::time_point time::_lastTime;
time_in_seconds time::_deltaTime;

void time::init()
{
	_startPoint = hr_clock::now();
	_lastTime = _startPoint;
	_deltaTime = time_in_seconds(0);
}

void time::tick()
{
	auto now = hr_clock::now();
	_deltaTime = std::chrono::duration_cast<time_in_seconds>(now - _lastTime);
	_lastTime = now;
}

time_in_seconds time::elapsedTime()
{
	return std::chrono::duration_cast<time_in_seconds>(hr_clock::now() - _startPoint);
}

time_in_seconds time::deltaTime()
{
	return _deltaTime;
}