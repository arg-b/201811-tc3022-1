#pragma once

#include <string>

class ifile
{
public:
	bool read(const std::string& filename);
	const std::string getContents() const;

private:
	std::string _contents;
};