#include "scene_model.h"

#include "time.h"

scene_model::scene_model()
{
	sPressed = wPressed = aPressed = dPressed = qPressed = ePressed = false;
}

void scene_model::init()
{
	myModel.load("nanosuit.obj");

	modelShader.create();
	modelShader.attachShader("PhongTexturedShadow.vert", GL_VERTEX_SHADER);
	modelShader.attachShader("PhongTexturedShadow.frag", GL_FRAGMENT_SHADER);
	modelShader.setAttribute(0, "VertexPosition");
	modelShader.setAttribute(1, "VertexColor");
	modelShader.setAttribute(2, "VertexNormal");
	modelShader.setAttribute(3, "VertexTexCoord");
	modelShader.link();

	sceneCamera.setPosition(0.0f, 7.5f, 15.0f);
}

void scene_model::awake()
{

}

void scene_model::mainLoop()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	myModel.modelTransform.setRotation(0.0f, 10.0f * time::elapsedTime().count(), 0.0f);

	modelShader.activate();
	modelShader.setUniformMatrix("mvpMatrix", sceneCamera.getProjectionMatrix() * sceneCamera.getViewMatrix() * myModel.modelTransform.getModelMatrix());
	modelShader.setUniformMatrix("modelMatrix", myModel.modelTransform.getModelMatrix());
	modelShader.setUniformMatrix("normalMatrix", mat3::transpose(mat3::inverse(mat3(myModel.modelTransform.getModelMatrix()))));
	modelShader.setUniformf("LightPosition", -5.0f, 5.0f, 5.0f);
	modelShader.setUniformf("LightColor", 1.0f, 1.0f, 1.0f);
	modelShader.setUniformf("LightPosition", -5.0f, 5.0f, 5.0f);
	modelShader.setUniformf("CameraPosition", 0.0f, 0.0f, 3.0f);
	modelShader.setUniformi("DiffuseTexture", 0);
	myModel.draw();
	modelShader.deactivate();

	float t = time::deltaTime().count();

	if (aPressed) sceneCamera.moveRight(-10.0f * t);
	if (dPressed) sceneCamera.moveRight(10.0f * t);
	if (wPressed) sceneCamera.moveForward(-10.0f * t);
	if (sPressed) sceneCamera.moveForward(10.0f * t);
	if (ePressed) sceneCamera.moveUp(10.0f * t);
	if (qPressed) sceneCamera.moveUp(-10.0f * t);
}

void scene_model::resize(int width, int height)
{
	glViewport(0, 0, width, height);
	sceneCamera.setPerspective(1.0f, 1000.0f, 60.0f, (float)width / (float)height);
}

void scene_model::normalKeysDown(unsigned char key)
{
	if (key == 'w') wPressed = true;
	if (key == 's') sPressed = true;
	if (key == 'a') aPressed = true;
	if (key == 'd') dPressed = true;
	if (key == 'q') qPressed = true;
	if (key == 'e') ePressed = true;
}

void scene_model::normalKeysUp(unsigned char key)
{
	if (key == 'w') wPressed = false;
	if (key == 's') sPressed = false;
	if (key == 'a') aPressed = false;
	if (key == 'd') dPressed = false;
	if (key == 'q') qPressed = false;
	if (key == 'e') ePressed = false;
}
