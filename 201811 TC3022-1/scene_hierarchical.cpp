#include "scene_hierarchical.h"

#include "time.h"

void scene_hierarchical::init()
{
	std::vector<vec3> positions;
	std::vector<vec3> colors;
	std::vector<unsigned int> indices = { 0, 1, 2, 0, 2, 3, 4, 5, 6, 4, 6, 7, 8, 9, 10, 8, 10, 11, 12, 13, 14, 12, 14, 15, 16, 17, 18, 16, 18, 19, 20, 21, 22, 20, 22, 23 };

	float x = 1.0f;
	float y = 0.3f;
	float z = 3.0f;

	// Cara frontal
	positions.push_back(vec3(-x, -y, z));
	positions.push_back(vec3(x, -y, z));
	positions.push_back(vec3(x, y, z));
	positions.push_back(vec3(-x, y, z));
	// Cara derecha
	positions.push_back(vec3(x, -y, z));
	positions.push_back(vec3(x, -y, -z));
	positions.push_back(vec3(x, y, -z));
	positions.push_back(vec3(x, y, z));
	// Cara trasera
	positions.push_back(vec3(x, -y, -z));
	positions.push_back(vec3(-x, -y, -z));
	positions.push_back(vec3(-x, y, -z));
	positions.push_back(vec3(x, y, -z));
	// Cara izquierda
	positions.push_back(vec3(-x, -y, -z));
	positions.push_back(vec3(-x, -y, z));
	positions.push_back(vec3(-x, y, z));
	positions.push_back(vec3(-x, y, -z));
	// Cara superior
	positions.push_back(vec3(-x, y, z));
	positions.push_back(vec3(x, y, z));
	positions.push_back(vec3(x, y, -z));
	positions.push_back(vec3(-x, y, -z));
	// Cara inferior
	positions.push_back(vec3(-x, -y, -z));
	positions.push_back(vec3(x, -y, -z));
	positions.push_back(vec3(x, -y, z));
	positions.push_back(vec3(-x, -y, z));

	colors.push_back(vec3(1.0f, 0.0f, 0.0f));
	colors.push_back(vec3(1.0f, 0.0f, 0.0f));
	colors.push_back(vec3(1.0f, 0.0f, 0.0f));
	colors.push_back(vec3(1.0f, 0.0f, 0.0f));

	colors.push_back(vec3(0.0f, 1.0f, 0.0f));
	colors.push_back(vec3(0.0f, 1.0f, 0.0f));
	colors.push_back(vec3(0.0f, 1.0f, 0.0f));
	colors.push_back(vec3(0.0f, 1.0f, 0.0f));

	colors.push_back(vec3(0.0f, 0.0f, 1.0f));
	colors.push_back(vec3(0.0f, 0.0f, 1.0f));
	colors.push_back(vec3(0.0f, 0.0f, 1.0f));
	colors.push_back(vec3(0.0f, 0.0f, 1.0f));

	colors.push_back(vec3(1.0f, 0.0f, 1.0f));
	colors.push_back(vec3(1.0f, 0.0f, 1.0f));
	colors.push_back(vec3(1.0f, 0.0f, 1.0f));
	colors.push_back(vec3(1.0f, 0.0f, 1.0f));

	colors.push_back(vec3(1.0f, 1.0f, 0.0f));
	colors.push_back(vec3(1.0f, 1.0f, 0.0f));
	colors.push_back(vec3(1.0f, 1.0f, 0.0f));
	colors.push_back(vec3(1.0f, 1.0f, 0.0f));

	colors.push_back(vec3(0.0f, 1.0f, 1.0f));
	colors.push_back(vec3(0.0f, 1.0f, 1.0f));
	colors.push_back(vec3(0.0f, 1.0f, 1.0f));
	colors.push_back(vec3(0.0f, 1.0f, 1.0f));

	cubeMesh.create(24);
	cubeMesh.setAttributePosition(positions, 0);
	cubeMesh.setAttributeColor(colors, 1);
	cubeMesh.setIndices(indices);

	cubeShader.create();
	cubeShader.attachShader("Default.vert", GL_VERTEX_SHADER);
	cubeShader.attachShader("Default.frag", GL_FRAGMENT_SHADER);
	cubeShader.setAttribute(0, "VertexPosition");
	cubeShader.setAttribute(1, "VertexColor");
	cubeShader.link();

	sceneCamera.setPosition(0.0f, 10.0f, 15.0f);
	sceneCamera.setRotation(-25.0f, 0.0f, 0.0f);

	cubeTransformJoint01.setPosition(0.0f, 0.0f, 0.0f);
	cubeTransformParent.setPosition(0.0f, 0.0f, 3.0f);
	cubeTransformJoint02.setPosition(0.0f, 0.0f, 3.0f);
	cubeTransformChild.setPosition(0.0f, 0.0f, 3.0f);

}

void scene_hierarchical::awake()
{
	glClearColor(1.0f, 0.4f, 0.1f, 1.0f);
}

void scene_hierarchical::mainLoop()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	float t = time::elapsedTime().count();
	cubeTransformJoint01.setRotation(0.0f, 10.0f * t, 0.0f);
	cubeShader.activate();
	cubeShader.setUniformMatrix("mvpMatrix", sceneCamera.getProjectionMatrix() * sceneCamera.getViewMatrix() * cubeTransformJoint01.getModelMatrix() * cubeTransformParent.getModelMatrix());
	cubeMesh.draw(GL_TRIANGLES);

	cubeTransformJoint02.setRotation(-120.0f * (sin(t) * 0.5f + 0.5f), 0.0f, 0.0f);
	cubeShader.setUniformMatrix("mvpMatrix", sceneCamera.getProjectionMatrix() * sceneCamera.getViewMatrix() * cubeTransformJoint01.getModelMatrix() * cubeTransformParent.getModelMatrix() * cubeTransformJoint02.getModelMatrix() * cubeTransformChild.getModelMatrix());
	cubeMesh.draw(GL_TRIANGLES);
	cubeShader.deactivate();
}

void scene_hierarchical::resize(int width, int height)
{
	glViewport(0, 0, width, height);
	sceneCamera.setPerspective(1.0f, 1000.0f, 60.0f, (float)width / (float)height);
}