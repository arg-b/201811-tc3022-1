#include "scene_manager.h"

int main(int argc, char* argv[])
{
	scene_manager application;
	application.start(argc, argv, "OpenGL", 400, 400);

	return 0;
}