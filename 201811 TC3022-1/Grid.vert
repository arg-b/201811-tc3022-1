void main()
{
  float sqrTotal = floor(sqrt(vertexCount));
  float width = sqrTotal;
  
  float x = mod(vertexId, width);    // [0, width - 1]
  float y = floor(vertexId / width); // [0, inf]

  // vertexId: 0 1 2 3 ... 10 11 12 13 ... 20 21 22 23 ... 100
  // mod    X: 0 1 2 3 ...  0  1  2  3 ...  0  1  2  3 ...   0
  // floor  Y: 0 0 0 0 ...  1  1  1  1 ...  2  2  2  2 ...  10
  
  float u = x / (width - 1.0);
  float v = y / (width - 1.0);
  
  float xOffset = cos(time + y * 0.2) * 0.1;
  float yOffset = cos(time + x * 0.3) * 0.2;
  
  float ux = u * 2.0 - 1.0 + xOffset;
  float vy = v * 2.0 - 1.0 + yOffset;
  
  vec2 xy = vec2(ux, vy) * 1.3;
  
  gl_Position = vec4(xy, 0.0, 1.0);
  
  float sizeOffset = sin(time + y * x * 0.02) * 5.0;
  
  gl_PointSize = 15.0 + sizeOffset;
  gl_PointSize *= 32.0 / width;
  
  v_color = vec4(1.0, 0.0, 0.0, 1.0);
}