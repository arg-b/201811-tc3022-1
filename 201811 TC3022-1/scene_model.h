#pragma once

#include "camera.h"
#include "scene.h"
#include "shader_program.h"

#include "model.h"

class scene_model : public scene
{
public:
	scene_model();
	~scene_model() {}

	void init();
	void awake();
	void sleep() { }
	void reset() { }
	void mainLoop();
	void resize(int width, int height);
	void normalKeysDown(unsigned char key);
	void normalKeysUp(unsigned char key);
	void specialKeys(int key) { }
	void passiveMotion(int x, int y) { }

private:
	model myModel;
	camera sceneCamera;
	shader_program modelShader;

	bool sPressed;
	bool wPressed;
	bool aPressed;
	bool dPressed;
	bool qPressed;
	bool ePressed;
};