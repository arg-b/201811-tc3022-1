#include "scene_mouserotation.h"

#include <GL/freeglut.h>
#include "time.h"

scene_mouserotation::scene_mouserotation()
{
	screenCenter.x = glutGet(GLUT_WINDOW_WIDTH) / 2;
	screenCenter.y = glutGet(GLUT_WINDOW_HEIGHT) / 2;

	smoothFactor = 0.25f;
}

void scene_mouserotation::init()
{
	std::vector<vec3> positions;
	std::vector<vec3> colors;
	std::vector<unsigned int> indices = { 0, 1, 2, 0, 2, 3, 4, 5, 6, 4, 6, 7, 8, 9, 10, 8, 10, 11, 12, 13, 14, 12, 14, 15, 16, 17, 18, 16, 18, 19, 20, 21, 22, 20, 22, 23 };

	float x = 3.0f;
	float y = 3.0f;
	float z = 3.0f;

	// Cara frontal
	positions.push_back(vec3(-x, -y, z));
	positions.push_back(vec3(x, -y, z));
	positions.push_back(vec3(x, y, z));
	positions.push_back(vec3(-x, y, z));
	// Cara derecha
	positions.push_back(vec3(x, -y, z));
	positions.push_back(vec3(x, -y, -z));
	positions.push_back(vec3(x, y, -z));
	positions.push_back(vec3(x, y, z));
	// Cara trasera
	positions.push_back(vec3(x, -y, -z));
	positions.push_back(vec3(-x, -y, -z));
	positions.push_back(vec3(-x, y, -z));
	positions.push_back(vec3(x, y, -z));
	// Cara izquierda
	positions.push_back(vec3(-x, -y, -z));
	positions.push_back(vec3(-x, -y, z));
	positions.push_back(vec3(-x, y, z));
	positions.push_back(vec3(-x, y, -z));
	// Cara superior
	positions.push_back(vec3(-x, y, z));
	positions.push_back(vec3(x, y, z));
	positions.push_back(vec3(x, y, -z));
	positions.push_back(vec3(-x, y, -z));
	// Cara inferior
	positions.push_back(vec3(-x, -y, -z));
	positions.push_back(vec3(x, -y, -z));
	positions.push_back(vec3(x, -y, z));
	positions.push_back(vec3(-x, -y, z));

	colors.push_back(vec3(1.0f, 0.0f, 0.0f));
	colors.push_back(vec3(1.0f, 0.0f, 0.0f));
	colors.push_back(vec3(1.0f, 0.0f, 0.0f));
	colors.push_back(vec3(1.0f, 0.0f, 0.0f));

	colors.push_back(vec3(0.0f, 1.0f, 0.0f));
	colors.push_back(vec3(0.0f, 1.0f, 0.0f));
	colors.push_back(vec3(0.0f, 1.0f, 0.0f));
	colors.push_back(vec3(0.0f, 1.0f, 0.0f));

	colors.push_back(vec3(0.0f, 0.0f, 1.0f));
	colors.push_back(vec3(0.0f, 0.0f, 1.0f));
	colors.push_back(vec3(0.0f, 0.0f, 1.0f));
	colors.push_back(vec3(0.0f, 0.0f, 1.0f));

	colors.push_back(vec3(1.0f, 0.0f, 1.0f));
	colors.push_back(vec3(1.0f, 0.0f, 1.0f));
	colors.push_back(vec3(1.0f, 0.0f, 1.0f));
	colors.push_back(vec3(1.0f, 0.0f, 1.0f));

	colors.push_back(vec3(1.0f, 1.0f, 0.0f));
	colors.push_back(vec3(1.0f, 1.0f, 0.0f));
	colors.push_back(vec3(1.0f, 1.0f, 0.0f));
	colors.push_back(vec3(1.0f, 1.0f, 0.0f));

	colors.push_back(vec3(0.0f, 1.0f, 1.0f));
	colors.push_back(vec3(0.0f, 1.0f, 1.0f));
	colors.push_back(vec3(0.0f, 1.0f, 1.0f));
	colors.push_back(vec3(0.0f, 1.0f, 1.0f));

	cubeMesh.create(24);
	cubeMesh.setAttributePosition(positions, 0);
	cubeMesh.setAttributeColor(colors, 1);
	cubeMesh.setIndices(indices);

	cubeShader.create();
	cubeShader.attachShader("Default.vert", GL_VERTEX_SHADER);
	cubeShader.attachShader("Default.frag", GL_FRAGMENT_SHADER);
	cubeShader.setAttribute(0, "VertexPosition");
	cubeShader.setAttribute(1, "VertexColor");
	cubeShader.link();

	sceneCamera.moveForward(10.0f);
}

void scene_mouserotation::awake()
{
	glClearColor(0.1f, 0.5f, 0.8f, 1.0f);
}

void scene_mouserotation::mainLoop()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	float t = time::elapsedTime().count();
	cubeTransform.setRotation(30.0f * t, 60.0f * t, 30.0f * t);

	cubeShader.activate();
	cubeShader.setUniformMatrix("mvpMatrix", sceneCamera.getProjectionMatrix() * sceneCamera.getViewMatrix() * cubeTransform.getModelMatrix());
	cubeMesh.draw(GL_TRIANGLES);
	cubeShader.deactivate();
}

void scene_mouserotation::resize(int width, int height)
{
	glViewport(0, 0, width, height);
	sceneCamera.setPerspective(1.0f, 1000.0f, 60.0f, (float)width / (float)height);

	screenCenter.x = width / 2;
	screenCenter.y = height / 2;
}

void scene_mouserotation::passiveMotion(int x, int y)
{
	if (x == screenCenter.x && y == screenCenter.y)
		return;

	glutWarpPointer(screenCenter.x, screenCenter.y);

	int diff = screenCenter.x - x;

	sceneCamera.yaw(diff * smoothFactor);
}
