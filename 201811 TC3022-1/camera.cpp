#include "camera.h"

camera::camera()
{
	setPerspective(1.0f, 1000.0f, 60.0f, 1.0f);
	_viewMatrix = mat4::inverse(_cameraTransform.getModelMatrix());
}

void camera::setPerspective(float near, float far, float fov, float aspect)
{
	_projectionMatrix = mat4(0.0f);

	float t = tan(cgmath::radians(fov * 0.5f));

	_projectionMatrix[0][0] = 1.0f / (aspect * t);
	_projectionMatrix[1][1] = 1.0f / t;
	_projectionMatrix[2][2] = -(far + near) / (far - near);
	_projectionMatrix[2][3] = -1.0f;
	_projectionMatrix[3][2] = -(2.0f * far * near) / (far - near);
}

void camera::setPosition(float x, float y, float z)
{
	_cameraTransform.setPosition(x, y, z);
	_viewMatrix = mat4::inverse(_cameraTransform.getModelMatrix());
}

void camera::setRotation(float x, float y, float z)
{
	_cameraTransform.setRotation(x, y, z);
	_viewMatrix = mat4::inverse(_cameraTransform.getModelMatrix());
}

void camera::moveForward(float delta)
{
	_cameraTransform.moveForward(delta);
	_viewMatrix = mat4::inverse(_cameraTransform.getModelMatrix());
}

void camera::moveRight(float delta)
{
	_cameraTransform.moveRight(delta);
	_viewMatrix = mat4::inverse(_cameraTransform.getModelMatrix());
}

void camera::moveUp(float delta)
{
	_cameraTransform.moveUp(delta);
	_viewMatrix = mat4::inverse(_cameraTransform.getModelMatrix());
}

void camera::yaw(float degrees)
{
	_cameraTransform.yaw(degrees);
	_viewMatrix = mat4::inverse(_cameraTransform.getModelMatrix());
}

mat4 camera::getProjectionMatrix() const
{
	return _projectionMatrix;
}

mat4 camera::getViewMatrix() const
{
	return _viewMatrix;
}