#pragma once

#include <chrono>

typedef std::chrono::steady_clock hr_clock;
typedef std::chrono::duration<float> time_in_seconds;

class time
{
public:
	static void init();
	static void tick();

	static time_in_seconds elapsedTime();
	static time_in_seconds deltaTime();

private:
	time();

	static hr_clock::time_point _startPoint;
	static hr_clock::time_point _lastTime;
	static time_in_seconds _deltaTime;
};