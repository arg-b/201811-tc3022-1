#include "transform.h"

transform::transform()
{
	_position = vec3(0.0f, 0.0f, 0.0f);
	_rotation = quat(vec3(0.0, 0.0, 0.0f));
	_scale = vec3(1.0f, 1.0f, 1.0f);

	_modelMatrix = mat4(1.0f);
}

void transform::setPosition(float x, float y, float z)
{
	_position.x = x;
	_position.y = y;
	_position.z = z;

	updateModelMatrixTranslation();
}

void transform::setRotation(float x, float y, float z)
{
	_rotation = quat(vec3(x, y, z));

	updateModelMatrixRotationScale();
}

void transform::setScale(float x, float y, float z)
{
	_scale.x = x;
	_scale.y = y;
	_scale.z = z;

	updateModelMatrixRotationScale();
}

void transform::moveForward(float delta)
{
	_position += cgmath::worldForward * delta;
	updateModelMatrixTranslation();
}

void transform::moveRight(float delta)
{
	_position += cgmath::worldRight * delta;
	updateModelMatrixTranslation();
}

void transform::moveUp(float delta)
{
	_position += cgmath::worldUp * delta;
	updateModelMatrixTranslation();
}

void transform::yaw(float degrees)
{
	_rotation = _rotation * quat(vec3(0.0f, degrees, 0.0f));
	updateModelMatrixRotationScale();
}

vec3 transform::getPosition() const
{
	return _position;
}

quat transform::getRotation() const
{
	return _rotation;
}

vec3 transform::getScale() const
{
	return _scale;
}

mat4 transform::getModelMatrix() const
{
	return _modelMatrix;
}

void transform::setModelMatrix(const mat4& modelMatrix)
{
	_modelMatrix = modelMatrix;

	_position.x = _modelMatrix[3][0];
	_position.y = _modelMatrix[3][1];
	_position.z = _modelMatrix[3][2];

	_rotation.fromMatrix(_modelMatrix);

	_scale = vec3(1.0f, 1.0f, 1.0f);
}

void transform::updateModelMatrixTranslation()
{
	_modelMatrix[3][0] = _position.x;
	_modelMatrix[3][1] = _position.y;
	_modelMatrix[3][2] = _position.z;
}

void transform::updateModelMatrixRotationScale()
{
	_modelMatrix = _rotation.getRotationMatrix();

	updateModelMatrixTranslation();

	_modelMatrix[0] *= _scale.x;
	_modelMatrix[1] *= _scale.y;
	_modelMatrix[2] *= _scale.z;
}
