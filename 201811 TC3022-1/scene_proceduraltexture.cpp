#include "scene_proceduraltexture.h"

#include <vector>
#include <random>

#include "cgmath.h"
#include "time.h"

void scene_proceduraltexture::init()
{
	std::vector<vec3> positions;
	positions.push_back(vec3(-1.0f, -1.0f, 0.0f));
	positions.push_back(vec3(1.0f, -1.0f, 0.0f));
	positions.push_back(vec3(1.0f, 1.0f, 0.0f));
	positions.push_back(vec3(-1.0f, 1.0f, 0.0f));
	std::vector<vec2> texCoords;
	texCoords.push_back(vec2(0.0f, 0.0f));
	texCoords.push_back(vec2(1.0f, 0.0f));
	texCoords.push_back(vec2(1.0f, 1.0f));
	texCoords.push_back(vec2(0.0f, 1.0f));
	std::vector<unsigned int> indices = { 0, 1, 2, 0, 2, 3 };

	planeMesh.create(positions.size());
	planeMesh.setAttributePosition(positions, 0);
	planeMesh.setAttributeTexCoord(texCoords, 1);
	planeMesh.setIndices(indices);

	planeShader.create();
	planeShader.attachShader("DefaultTextured.vert", GL_VERTEX_SHADER);
	planeShader.attachShader("DefaultTextured.frag", GL_FRAGMENT_SHADER);
	planeShader.setAttribute(0, "VertexPosition");
	planeShader.setAttribute(1, "VertexTexCoord");
	planeShader.link();

	planeShader.activate();
	planeShader.setUniformi("DiffuseTexture", 0);
	planeShader.deactivate();

	std::default_random_engine generator;
	std::uniform_real_distribution<float> distribution(0.0f, 1.0f);

	std::vector<vec3> pixels;
	//vec3 pixels[200][200]; // Ejemplo con arreglos
	for (int i = 0; i < 200; i++)
	{
		for (int j = 0; j < 200; j++)
		{
			float r = distribution(generator);
			float g = distribution(generator);
			float b = distribution(generator);
			pixels.push_back(vec3(r, g, b));
			//pixels[i][j] = vec3(r, g, b); // Ejemplo con arreglos
		}
	}
	planeTexture.loadTexture(pixels, 200, 200);
	//planeTexture.loadTexture(pixels); // Ejemplo con arreglos
}

void scene_proceduraltexture::mainLoop()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	planeShader.activate();
	planeShader.setUniformMatrix("mvpMatrix", mat4(1.0f));

	glActiveTexture(GL_TEXTURE0);
	planeTexture.bind();

	planeMesh.draw(GL_TRIANGLES);

	glActiveTexture(GL_TEXTURE0);
	planeTexture.unbind();

	planeShader.deactivate();
}

void scene_proceduraltexture::resize(int width, int height)
{
	glViewport(0, 0, width, height);
}
