#include "vec4.h"

vec4::vec4()
{
	x = y = z = w = 0.0f;
}

vec4::vec4(float x, float y, float z, float w)
{
	this->x = x;
	this->y = y;
	this->z = z;
	this->w = w;
}

float& vec4::operator[](int i)
{
	return (&x)[i];
}

const float& vec4::operator[](int i) const
{
	return (&x)[i];
}

vec4& vec4::operator*=(float s)
{
	x *= s;
	y *= s;
	z *= s;
	w *= s;
	return *this;
}

vec4& vec4::operator/=(float s)
{
	s = 1.0f / s;
	x *= s;
	y *= s;
	z *= s;
	w *= s;
	return *this;
}

vec4& vec4::operator+=(const vec4& v)
{
	x += v.x;
	y += v.y;
	z += v.z;
	w += v.w;
	return *this;
}

vec4& vec4::operator-=(const vec4& v)
{
	x -= v.x;
	y -= v.y;
	z -= v.z;
	w -= v.w;
	return *this;
}

bool vec4::operator==(const vec4& v) const
{
	return x == v.x && y == v.y && z == v.z && w == v.w;
}
