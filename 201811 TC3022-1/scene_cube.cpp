#include "scene_cube.h"

#include "cgmath.h"
#include "time.h"
#include <GL/freeglut.h>
#include <vector>

scene_cube::scene_cube()
{
	wPressed = sPressed = dPressed = aPressed = false;
}

void scene_cube::init()
{
	std::vector<vec3> positions;
	std::vector<vec3> colors;
	std::vector<unsigned int> indices = { 0, 1, 2, 0, 2, 3, 4, 5, 6, 4, 6, 7, 8, 9, 10, 8, 10, 11, 12, 13, 14, 12, 14, 15, 16, 17, 18, 16, 18, 19, 20, 21, 22, 20, 22, 23 };

	float x = 3.0f;
	float y = 3.0f;
	float z = 3.0f;

	// Cara frontal
	positions.push_back(vec3(-x, -y, z));
	positions.push_back(vec3(x, -y, z));
	positions.push_back(vec3(x, y, z));
	positions.push_back(vec3(-x, y, z));
	// Cara derecha
	positions.push_back(vec3(x, -y, z));
	positions.push_back(vec3(x, -y, -z));
	positions.push_back(vec3(x, y, -z));
	positions.push_back(vec3(x, y, z));
	// Cara trasera
	positions.push_back(vec3(x, -y, -z));
	positions.push_back(vec3(-x, -y, -z));
	positions.push_back(vec3(-x, y, -z));
	positions.push_back(vec3(x, y, -z));
	// Cara izquierda
	positions.push_back(vec3(-x, -y, -z));
	positions.push_back(vec3(-x, -y, z));
	positions.push_back(vec3(-x, y, z));
	positions.push_back(vec3(-x, y, -z));
	// Cara superior
	positions.push_back(vec3(-x, y, z));
	positions.push_back(vec3(x, y, z));
	positions.push_back(vec3(x, y, -z));
	positions.push_back(vec3(-x, y, -z));
	// Cara inferior
	positions.push_back(vec3(-x, -y, -z));
	positions.push_back(vec3(x, -y, -z));
	positions.push_back(vec3(x, -y, z));
	positions.push_back(vec3(-x, -y, z));

	colors.push_back(vec3(1.0f, 0.0f, 0.0f));
	colors.push_back(vec3(1.0f, 0.0f, 0.0f));
	colors.push_back(vec3(1.0f, 0.0f, 0.0f));
	colors.push_back(vec3(1.0f, 0.0f, 0.0f));

	colors.push_back(vec3(0.0f, 1.0f, 0.0f));
	colors.push_back(vec3(0.0f, 1.0f, 0.0f));
	colors.push_back(vec3(0.0f, 1.0f, 0.0f));
	colors.push_back(vec3(0.0f, 1.0f, 0.0f));

	colors.push_back(vec3(0.0f, 0.0f, 1.0f));
	colors.push_back(vec3(0.0f, 0.0f, 1.0f));
	colors.push_back(vec3(0.0f, 0.0f, 1.0f));
	colors.push_back(vec3(0.0f, 0.0f, 1.0f));

	colors.push_back(vec3(1.0f, 0.0f, 1.0f));
	colors.push_back(vec3(1.0f, 0.0f, 1.0f));
	colors.push_back(vec3(1.0f, 0.0f, 1.0f));
	colors.push_back(vec3(1.0f, 0.0f, 1.0f));

	colors.push_back(vec3(1.0f, 1.0f, 0.0f));
	colors.push_back(vec3(1.0f, 1.0f, 0.0f));
	colors.push_back(vec3(1.0f, 1.0f, 0.0f));
	colors.push_back(vec3(1.0f, 1.0f, 0.0f));

	colors.push_back(vec3(0.0f, 1.0f, 1.0f));
	colors.push_back(vec3(0.0f, 1.0f, 1.0f));
	colors.push_back(vec3(0.0f, 1.0f, 1.0f));
	colors.push_back(vec3(0.0f, 1.0f, 1.0f));

	cubeMesh.create(24);
	cubeMesh.setAttributePosition(positions, 0);
	cubeMesh.setAttributeColor(colors, 1);
	cubeMesh.setIndices(indices);

	cubeShader.create();
	cubeShader.attachShader("Default.vert", GL_VERTEX_SHADER);
	cubeShader.attachShader("Default.frag", GL_FRAGMENT_SHADER);
	cubeShader.setAttribute(0, "VertexPosition");
	cubeShader.setAttribute(1, "VertexColor");
	cubeShader.link();

	sceneCamera.moveForward(10.0f);
}

void scene_cube::awake()
{
	glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
}

void scene_cube::mainLoop()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	float t = time::elapsedTime().count();
	cubeTransform.setRotation(30.0f * t, 60.0f * t, 30.0f * t);

	cubeShader.activate();
	cubeShader.setUniformMatrix("mvpMatrix", sceneCamera.getProjectionMatrix() * sceneCamera.getViewMatrix() * cubeTransform.getModelMatrix());
	cubeMesh.draw(GL_TRIANGLES);
	cubeShader.deactivate();

	if (wPressed)
		sceneCamera.moveForward(-10.0f * time::deltaTime().count());
	if (dPressed)
		sceneCamera.moveRight(10.0f * time::deltaTime().count());
	if (aPressed)
		sceneCamera.moveRight(-10.0f * time::deltaTime().count());
	if (sPressed)
		sceneCamera.moveForward(10.0f * time::deltaTime().count());
}

void scene_cube::resize(int width, int height)
{
	glViewport(0, 0, width, height);
	sceneCamera.setPerspective(1.0f, 1000.0f, 60.0f, (float)width / (float)height);
}

void scene_cube::normalKeysDown(unsigned char key)
{
	if (key == 'w')
	{
		wPressed = true;
	}
	if (key == 's')
	{
		sPressed = true;
	}
	if (key == 'd')
	{
		dPressed = true;
	}
	if (key == 'a')
	{
		aPressed = true;
	}
}

void scene_cube::normalKeysUp(unsigned char key)
{
	if (key == 'w')
	{
		wPressed = false;
	}
	if (key == 's')
	{
		sPressed = false;
	}
	if (key == 'd')
	{
		dPressed = false;
	}
	if (key == 'a')
	{
		aPressed = false;
	}
}

void scene_cube::specialKeys(int key)
{
	if (key == GLUT_KEY_UP)
	{
		sceneCamera.moveForward(-10.0f);
	}
	if (key == GLUT_KEY_DOWN)
	{
		sceneCamera.moveForward(10.0f);
	}
}
