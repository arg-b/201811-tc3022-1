#pragma once

#pragma once

#include "camera.h"
#include "mesh.h"
#include "scene.h"
#include "shader_program.h"
#include "tex2d.h"
#include "transform.h"

class scene_proceduraltexture : public scene
{
public:
	scene_proceduraltexture() { }
	~scene_proceduraltexture() { }

	void init();
	void awake() { }
	void sleep() { }
	void reset() { }
	void mainLoop();
	void resize(int width, int height);
	void normalKeysDown(unsigned char key) { }
	void normalKeysUp(unsigned char key) { }
	void specialKeys(int key) { }
	void passiveMotion(int x, int y) { }

private:
	mesh planeMesh;
	shader_program planeShader;
	tex2d planeTexture;
};